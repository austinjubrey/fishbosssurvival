using UnityEngine;
using UnityEngine.Events;

public enum EStatus
{
    Stun,
    Poison
}

public class Enemy : Unit
{
    private EEnemyID _enemyID = EEnemyID.BaseEnemy;
    private float _moveSpeed = 1.5f;
    private float _meleeDamage = 2f;
    private float _catchUpDistance = 15f;
    private float _catchUpSpeedMultiplier = 2f;
    private Player _target;

    private bool _isHooked;
    private GameObject _hook;
    private bool _isFacingLeft;

    [SerializeField] private BoxColliderHelper _collisionHelper;
    [SerializeField] private DamageDisplay _damageDisplay;
    
    [SerializeField] private GameObject _deathLoot;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private AnimationHandler _animationHandler;
    private UnityAction<Enemy> _deathCallback;

    public void SetTarget(Player player)
    {
        _target = player;
    }

    public EEnemyID GetEnemyID()
    {
        return _enemyID;
    }

    public void SetupFromData(EnemyData data)
    {
        _catchUpDistance = SaveManager.Instance.GetEconomyData().EnemyCatchUpDistance;
        _catchUpSpeedMultiplier = SaveManager.Instance.GetEconomyData().EnemyCatchUpMultiplier;
        _animationHandler.SetTexture(data.Texture);
        _enemyID = data.EnemyID;
        _maxHealth = data.Health;
        _currentHealth = _maxHealth;
        _moveSpeed = data.MoveSpeed;
        _meleeDamage = data.MeleeDamage;
    }

    public void ApplyMultipliers(float healthMultiplier, float damageMultiplier)
    {
        if (healthMultiplier == 0 || damageMultiplier == 0)
            return;
        _maxHealth *= healthMultiplier;
        _currentHealth = _maxHealth;
        _meleeDamage *= damageMultiplier;
    }

    public void SetDeathCallback(UnityAction<Enemy> callback)
    {
        _deathCallback = callback;
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
        
        ManageDirection();
        
        if(!_isHooked)
            Move();
        else
            HookedMovement();
    }

    private void ManageDirection()
    {
        if (_target == null)
            return;

        if (!_isFacingLeft && _target.transform.position.x < transform.position.x)
        {
            _spriteRenderer.flipY = true;
            _isFacingLeft = true;
        }
        else if (_isFacingLeft && _target.transform.position.x > transform.position.x)
        {
            _spriteRenderer.flipY = false;
            _isFacingLeft = false;
        }
        
        Vector2 direction = _target.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(Vector3.forward * angle);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Projectile")
        {
            //ReduceHealth(other.gameObject.GetComponent<BasicProjectile>().GetDamage());
        }
    }

    public float GetMeleeDamage()
    {
        return _meleeDamage;
    }

    public void OnHit(float damage)
    {
        _damageDisplay.SetAndPlay(damage);
        TakeDamage(damage);
    }

    protected override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        
        if (_currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        DropLoot();
        _damageDisplay.DetachAndDestroy();
        _deathCallback(this);
        PiersEvent.Post(PiersEventKey.EventKey.EnemyKilled, _enemyID);
        Destroy(gameObject);
    }

    private void DropLoot()
    {
        if(Random.Range(0,4) == 0)
            Instantiate(_deathLoot, transform.position, Quaternion.identity);
    }

    private void Move()
    {
        if (HasStatus(EStatus.Stun))
            return;
        
        if (_target == null)
            return;

        var speed = Vector3.Distance(_target.transform.position, transform.position) > _catchUpDistance
            ? _moveSpeed * _catchUpSpeedMultiplier
            : _moveSpeed;
        
        transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
            new Vector2(_target.transform.position.x, _target.transform.position.y), speed * Time.deltaTime);
    }

    public void AttachHook(GameObject hook)
    {
        _isHooked = true;
        _hook = hook;
        transform.SetParent(hook.transform);
        GetComponent<Rigidbody2D>().simulated = false;
    }

    public void DetachHook()
    {
        _isHooked = false;
        _hook = null;
        transform.SetParent(null);
        GetComponent<Rigidbody2D>().simulated = true;
    }

    private void HookedMovement()
    {
        //maybe they wiggle or something
    }
}
