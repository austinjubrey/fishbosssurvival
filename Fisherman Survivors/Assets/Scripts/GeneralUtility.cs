using UnityEngine;

public static class GeneralUtility
{
    public static void UnParentAndDestroyChildren(Transform content)
    {
        int originalCount = content.transform.childCount;

        for (int i = originalCount - 1; i >= 0; i--)
        {
            Transform child = content.transform.GetChild(i);
            child.gameObject.SetActive(false); //This way it won't trigger OnEnabled when unParenting
            child.SetParent(null);
            GameObject.Destroy(child.gameObject);
        }
    }
}
