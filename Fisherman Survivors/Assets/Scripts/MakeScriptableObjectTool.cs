﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using System.Collections.Generic;

#if UNITY_EDITOR
public class MakeScriptableObject
{
    [MenuItem("Assets/Create/CustomData/Weapon Data")]
    public static void CreateWeaponDataAsset()
    {
        WeaponData asset = ScriptableObject.CreateInstance<WeaponData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/WeaponData/WeaponData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Wave Data")]
    public static void CreateWaveDataAsset()
    {
        WaveData asset = ScriptableObject.CreateInstance<WaveData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/WaveData/WaveData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Experience Data")]
    public static void CreateExperienceDataAsset()
    {
        ExperienceData asset = ScriptableObject.CreateInstance<ExperienceData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/ExperienceData/ExperienceData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Economy Data")]
    public static void CreateEconomyDataAsset()
    {
        EconomyData asset = ScriptableObject.CreateInstance<EconomyData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/ExperienceData/EconomyData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Shop Item Data")]
    public static void CreateShopItemDataAsset()
    {
        ShopItemData asset = ScriptableObject.CreateInstance<ShopItemData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/ShopItemData/ShopItemData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Tool Data")]
    public static void CreateToolDataAsset()
    {
        ToolData asset = ScriptableObject.CreateInstance<ToolData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/ToolData/ToolData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Enemy Data")]
    public static void CreateEnemyDataAsset()
    {
        EnemyData asset = ScriptableObject.CreateInstance<EnemyData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/EnemyData/EnemyData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
    
    [MenuItem("Assets/Create/CustomData/Character Data")]
    public static void CreateCharacterDataAsset()
    {
        CharacterData asset = ScriptableObject.CreateInstance<CharacterData>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/CharacterData/CharacterData.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
#endif