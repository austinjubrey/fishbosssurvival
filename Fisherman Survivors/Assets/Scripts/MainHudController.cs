using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainHudController : MonoBehaviour
{
    [SerializeField] private GameObject _topHudContainer;
    [SerializeField] private Image _healthBarFill;
    [SerializeField] private Image _XPBarFill;
    [SerializeField] private TextMeshProUGUI _XPLabel;
    [SerializeField] private LevelUpMenuController _levelUpMenu;
    [SerializeField] private StatsContainerController _statsController;
    [SerializeField] private GameObject _gameOverParent;
    [SerializeField] private GameObject _pauseMenuParent;
    [SerializeField] private GameObject _haulReportMenu;
    [SerializeField] private Animator _xpBarAnimator;
    
    [SerializeField] private List<GameObject> _countDownImages;

    private static string MAIN_MENU_SCENE_NAME = "MainMenu";

    private float _maxHealth;
    private Player _player;
    private bool _isPaused;
    
    // Start is called before the first frame update
    void Start()
    {
        _healthBarFill.fillMethod = Image.FillMethod.Horizontal;
        _healthBarFill.fillClockwise = true;
        Listen();
        Setup();
    }

    private void Listen()
    {
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerTakesDamage, UpdateHealthBar);
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerGainsXP, UpdateXP_Bar);
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerSpawned, Setup);
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerLevelUp, OnLevelUp);
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerDied, ShowGameOver);
        PiersEvent.Listen(PiersEventKey.EventKey.TransitionToGameComplete, OnTransitionToGameComplete);
    }

    private void Setup()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _maxHealth = _player.GetMaxHealth();
        UpdateAll();
        _statsController.SetStatsActive(false);
    }

    private void OnTransitionToGameComplete()
    {
        StartCoroutine(nameof(GameStartCountDown));
    }

    private IEnumerator GameStartCountDown()
    {
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(1f);

            if (i > 0)
            {
                _countDownImages[i - 1].SetActive(false);
            }
            else
                _topHudContainer.SetActive(true);
            
            if (i == 3)
                break;

            _countDownImages[i].SetActive(true);
        }
        PiersEvent.Post(PiersEventKey.EventKey.GameStart);
    }

    public void GoToMainMenu()
    {
        PiersEvent.Post(PiersEventKey.EventKey.GameEnded);
        ResumeGame();
        SceneManager.LoadScene(MAIN_MENU_SCENE_NAME);
    }

    private void ShowGameOver()
    {
        _gameOverParent.SetActive(true);
        _statsController.SetStatsActive(true);
    }

    public void ShowHaulReport()
    {
        _haulReportMenu.GetComponent<HaulReportController>().Setup();
        _haulReportMenu.SetActive( true);
        _gameOverParent.SetActive(false);
        _statsController.SetStatsActive(false);
    }

    private void DisplayPauseMenu(bool display)
    {
        _pauseMenuParent.SetActive(display);
    }

    public void RequestPause()
    {
        PauseGame(true);
    }

    private void PauseGame (bool showOptions)
    {
        Time.timeScale = 0;
        _statsController.SetStatsActive(true);
        DisplayPauseMenu(showOptions);
        _isPaused = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        _statsController.SetStatsActive(false);
        DisplayPauseMenu(false);
        _isPaused = false;
    }

    private void UpdateAll()
    {
        UpdateHealthBar();
        UpdateXP_Bar();
    }

    private void UpdateHealthBar()
    {
        _healthBarFill.fillAmount = _player.GetHealth() / _maxHealth;
    }
    
    private void UpdateXP_Bar()
    {
        _xpBarAnimator.SetTrigger("shake");
        UpdateLevelLabel();
        _XPBarFill.fillAmount = _player.GetCurrentXP() / _player.GetMaxXP();
    }

    private void OnLevelUp()
    {
        UpdateLevelLabel();
        
        if (_player.HasMaxNumWeapons() && !_player.HasAvailableWeaponUpgrades())
            return;
        
        PauseGame(false);
        _levelUpMenu.gameObject.SetActive(true);
        _levelUpMenu.Setup(OnSelectionMade);
    }

    private void OnSelectionMade()
    {
        ResumeGame();
    }

    private void UpdateLevelLabel()
    {
        _XPLabel.text = _player.GetLevel().ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(!_isPaused)
                RequestPause();
            else
                ResumeGame();
        }
    }
}
