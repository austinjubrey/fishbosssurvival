using TMPro;
using UnityEngine;

public class HaulReportController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _totalGoldLabel;
    [SerializeField] private TextMeshProUGUI _goldPickupLabel;
    [SerializeField] private TextMeshProUGUI _bonusValueLabel;
    [SerializeField] private Transform _goldEarnedItemContent;
    [SerializeField] private GameObject _haulReportItemPrefab;

    public void Setup()
    {
        PopulateContent();
        
        _goldPickupLabel.text = GameStatTracker.Instance.GetGoldPickedUp().ToString();
    }

    private void PopulateContent()
    {
        GeneralUtility.UnParentAndDestroyChildren(_goldEarnedItemContent);

        var deathLog = GameStatTracker.Instance.GetDeathLog();
        int totalEarned = 0;
        foreach (var enemyInfo in deathLog)
        {
            var newItem = Instantiate(_haulReportItemPrefab, _goldEarnedItemContent);
            HaulReportItem newReportItem = newItem.GetComponent<HaulReportItem>();
            EnemyData data = DataLibrary.Instance.GetEnemyDataByID(enemyInfo.Key);
            newReportItem.SetImage(data.DisplaySprite);
            newReportItem.SetAmountLabel(enemyInfo.Value);
            int totalValue = data.GoldValue * enemyInfo.Value;
            newReportItem.SetTotalLabel(totalValue);
            totalEarned += totalValue;
        }

        var OverallGoldEarned = GameStatTracker.Instance.GetGoldPickedUp() + totalEarned;
        var GoldIncreaseAmount = SaveManager.Instance.GetUpgradedGoldIncreaseValue() * 0.01f;
        var BonusAmount = GoldIncreaseAmount * OverallGoldEarned;
        var GoldWithIncrease = OverallGoldEarned + BonusAmount;
        _bonusValueLabel.text = "(" + OverallGoldEarned + " + " + SaveManager.Instance.GetUpgradedGoldIncreaseValue() + "% bonus)";
        _totalGoldLabel.text = OverallGoldEarned.ToString();
        
        SaveManager.Instance.GetPlayerSave().AddGold((int)GoldWithIncrease);
        SaveManager.Instance.SaveGame();
    }
}