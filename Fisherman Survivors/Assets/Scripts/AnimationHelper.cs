using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationHelper : MonoBehaviour
{
    private UnityAction _callback;

    public void SetCallback(UnityAction callback)
    {
        _callback = callback;
    }

    public void OnAnimationEnd()
    {
        if (_callback != null)
            _callback();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
