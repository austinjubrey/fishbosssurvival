using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootPickup : MonoBehaviour
{
    [SerializeField] protected SpriteRenderer _spriteRenderer;
    [SerializeField] protected BoxColliderHelper _collisionHelper;
    
    protected float _maxLifeTime = 45f;
    protected float _currentLifeTime;
    protected int _goldValue = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.SetParent(null);
        _collisionHelper.SetTriggerCallback(OnTriggerEnter2D);
    }

    // Update is called once per frame
    void Update()
    {
        ManageLifeTime();
    }
    
    protected void ManageLifeTime()
    {
        _currentLifeTime += Time.deltaTime;
        if (_currentLifeTime >= _maxLifeTime)
        {
            OnEndLifeTime();
        }
    }
    
    protected void OnEndLifeTime()
    {
        Destroy(gameObject);
    }

    private void OnCollect()
    {
        //give player money, or whatever
        PiersEvent.Post(PiersEventKey.EventKey.LootCollected);
        OnEndLifeTime();
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null || other.gameObject.tag.Equals("Enemy"))
            return;
        
        var player = other.gameObject.GetComponentInParent<Player>();
        var objectTag = other.gameObject.tag;
        if (!objectTag.Equals("Player") || player == null)
            return;

        OnCollect();
    }
}
