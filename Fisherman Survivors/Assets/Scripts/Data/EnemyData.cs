using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EEnemyID
{
    BaseEnemy = 0,
    lilGreen,
    BrownBlobby,
    BabyShark
}

public class EnemyData : ScriptableObject
{
    public EEnemyID EnemyID;
    public string Name;
    public string Description;
    public Texture2D Texture;
    public Sprite DisplaySprite;
    public int GoldValue;
    public float Health;
    public float MoveSpeed;
    public float MeleeDamage;
}
