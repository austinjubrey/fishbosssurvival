using UnityEngine;

[System.Serializable]
public enum ECharacterID
{
    Fisherman = 0,
    FishPerson,
}

public class CharacterData : ScriptableObject
{
    public string CharacterName;
    public ECharacterID CharacterID;
    public EWeaponID DefaultWeaponID;
    public Sprite Sprite;
    public Texture2D SwimmingSprites;
    public Sprite MainMenuSprite;
    [Tooltip("Base health for player")][Min(1)]public float CharacterBaseHealth;
    [Tooltip("Base speed for player")][Min(1)]public float CharacterMovementSpeed;
    [Tooltip("Bonus Damage")]public float CharacterDamage;
    [Tooltip("Bonus Fire (eg. -0.25)")]public float CharacterFireRate;
    [Tooltip("Bonus Range")]public float CharacterRange;
    [Tooltip("Bonus Projectile Size")]public float CharacterSizeModifier;
    [Tooltip("Bonus Projectiles")]public int CharacterExtraProjectiles;
    [Tooltip("Bonus Enemy Pierce")]public int CharacterEnemyPierce;
}
