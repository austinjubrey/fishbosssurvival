
using System.Linq;

public static class WeaponDataHelper
{
    public static float GetBattleItemStatAtLevel(BattleItemData data, int level, EUpgradeType stat)
    {
        int numStatFound = 0;
        if (level > data._levelUpInfo._upgradeTiers.Count - 1)
        {
            foreach (var upgradeType in data._levelUpInfo._upgradeTiers.Last()._upgrades)
            {
                if (upgradeType == stat)
                {
                    numStatFound++;
                }
            }

            return numStatFound;
        }

        for (int i = 0; i < level; i++)
        {
            if (data._levelUpInfo._upgradeTiers[i] != null)
            {
                for (int j = 0; j < data._levelUpInfo._upgradeTiers[i]._upgrades.Count; j++)
                {
                    if (data._levelUpInfo._upgradeTiers[i]._upgrades[j] == stat)
                    {
                        numStatFound++;
                    }
                }
            }
        }

        return numStatFound;
    }
}
