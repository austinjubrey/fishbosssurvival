using UnityEngine;

public enum ShopItemID
{
    fishingLineUpgrade,
    fishingHook,
    healthUpgrade,
    damageUpgrade,
    goldIncreaseUpgrade,
    xpIncreaseUpgrade,
    movementSpeedUpgrade
}

public class ShopItemData : ScriptableObject
{
    public string Title;
    public Sprite Image;
    public ShopItemID ID;
    public int Cost;
}
