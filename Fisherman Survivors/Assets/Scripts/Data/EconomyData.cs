using UnityEngine;

public class EconomyData : ScriptableObject
{
    [Header("Enemy Values")]
    [Tooltip("When the enemy is > this distance away, we apply a multiplier to their speed")]public float EnemyCatchUpDistance;
    [Tooltip("Multiplier applied to enemy speed when enemy is far away from the player")]public float EnemyCatchUpMultiplier;
    
    [Header("Status Conditions")]
    [Tooltip("How long a Stun lasts when applied (in Seconds)")]public float DefaultStunDuration;
    [Tooltip("How long Poison lasts when applied (in Seconds)")]public float DefaultPoisonDuration;
    [Tooltip("How much damage poison does per tick")]public float DefaultPoisonDamagePerTick;
    [Tooltip("How long a tick of poison is (in Seconds)")]public float PoisonTickFrequency;
    
    [Header("Upgrades / Unlocks")]
    [Tooltip("Each upgraded stat will be multiplied by this when applied to the weapon")]public float DefaultWeaponUpgradeValue; //could be done per damage, fire rate, etc
    [Tooltip("Amount of extra length of fishing line per store upgrade")]public float FishingLineUpgradeValue;
    [Tooltip("Amount of times fishing line can be bought")]public int FishingLineUpgradeMax;
    [Tooltip("Each level of fire rate on a weapon will make it attack this much faster")]public float FireRateUpgradeValue;
    [Tooltip("Each level of Size Modifier on a weapon will make larger")]public float SizeModifierUpgradeValue;
    [Tooltip("Each health upgrade bought in the shop increases health by this much (eg. 10 = 10%)")]public float HealthUpgradeValue;
    [Tooltip("Amount of times Health upgrade can be bought")]public int HealthUpgradeMax;
    [Tooltip("Each damage upgrade bought in the shop increases damage by this much (eg. 10 = 10%)")]public float DamageUpgradeValue;
    [Tooltip("Amount of times damage upgrade can be bought")]public int DamageUpgradeMax;
    [Tooltip("Each Gold Increase upgrade bought in the shop increases Gold Increase by this much (eg. 10 = 10%)")]public float GoldIncreaseUpgradeValue;
    [Tooltip("Amount of times Gold Increase upgrade can be bought")]public int GoldIncreaseUpgradeMax;
    [Tooltip("Each XP Increase upgrade bought in the shop increases XP Increase by this much (eg. 10 = 10%)")]public float XpIncreaseUpgradeValue;
    [Tooltip("Amount of times XP Increase upgrade can be bought")]public int XpIncreaseUpgradeMax;
    [Tooltip("Each movement speed Increase upgrade bought in the shop increases movement speed Increase by this much (eg. 10 = 10%)")]public float MovementSpeedUpgradeValue;
    [Tooltip("Amount of times movement speed Increase upgrade can be bought")]public int MovementSpeedUpgradeMax;
    [Tooltip("Seconds that a ThrowNet must exist before it can deploy on an enemy")]public float DefaultThrowNetDeployTime;

}
