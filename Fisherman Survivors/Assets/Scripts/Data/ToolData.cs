using UnityEngine;

public enum EToolID
{
    TackleBox,
    BlastPlate,
    HarpoonOil,
    HookSharpener,
}

public class ToolData : BattleItemData
{
    public EToolID ToolID;
    public Sprite ToolSprite;
}
