using System.Collections.Generic;
using UnityEngine;

public class WaveData : ScriptableObject
{
    public List<WaveInfo> _waveInfo;
}
