using UnityEngine;

public class BattleItemData : ScriptableObject
{
    public string Name;
    public string Description;
    public float BaseDamage;
    public float BaseFireRate;
    public float BaseRange;
    public float BaseSizeModifier;
    public int BaseExtraProjectiles;
    public int BaseEnemyPierce;
    
    public LevelUpTier _levelUpInfo = new LevelUpTier();
}
