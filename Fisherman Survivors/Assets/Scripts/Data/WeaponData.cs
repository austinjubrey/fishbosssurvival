using UnityEngine;

public enum EUpgradeType
{
    Damage,
    FireRate,
    Range,
    SizeModifier,
    ExtraProjectiles,
    ExtraPierce
}

public enum EWeaponID
{
    FishingRod,
    Harpoon,
    Net,
    BasicTestWeapon,
    Trident,
    WhirlyRod,
    ThrowNet,
    Anchor,
    SeaweedSword
}

public class WeaponData : BattleItemData
{
    public EWeaponID WeaponID;
    public Sprite WeaponSprite;
    public GameObject ProjectilePrefab;
}