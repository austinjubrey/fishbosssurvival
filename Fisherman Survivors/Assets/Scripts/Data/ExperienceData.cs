using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceData : ScriptableObject
{
    public List<float> _experienceThresholds;
}
