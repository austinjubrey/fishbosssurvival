using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleLabel;
    [SerializeField] private TextMeshProUGUI _quantityLabel;
    [SerializeField] private TextMeshProUGUI _costLabel;
    [SerializeField] private Image _image;
    [SerializeField] private Button _button;

    private float _cost;
    private ShopItemData _data;

    private UnityAction _purchaseCallback;

    private void SetQuantityLabel(float current, float max)
    {
        _quantityLabel.text = current + "/" + max;
    }

    private void SetTitleLabel(string name)
    {
        _titleLabel.text = name;
    }
    
    private void SetCostLabel(string cost)
    {
        _costLabel.text = cost;
    }
    
    private void SetImage(Sprite name)
    {
        _image.sprite = name;
    }

    private void RefreshUi()
    {
        SetupByID();
    }

    //this should include a function for every shopItemData
    private void SetupByID()
    {
        var playerSave = SaveManager.Instance.GetPlayerSave();
        var economyData = SaveManager.Instance.GetEconomyData();
        switch (_data.ID)
        {
            case ShopItemID.fishingLineUpgrade:
                SetupButton(playerSave.GetFishingLineUpgrades(), economyData.FishingLineUpgradeMax, playerSave.AddFishingLineUpgrade);
                break;
            case ShopItemID.fishingHook:
                SetupButton(playerSave.GetHasUnlockedHook() ? 1 : 0, 1, playerSave.UnlockHook);
                break;
            case ShopItemID.healthUpgrade:
                SetupButton(playerSave.GetHealthUpgrades(), economyData.HealthUpgradeMax, playerSave.AddHealthUpgrade);
                break;
            case ShopItemID.damageUpgrade:
                SetupButton(playerSave.GetDamageUpgrades(), economyData.DamageUpgradeMax, playerSave.AddDamageUpgrade);
                break;
            case ShopItemID.goldIncreaseUpgrade:
                SetupButton(playerSave.GetGoldIncreaseUpgrades(), economyData.GoldIncreaseUpgradeMax, playerSave.AddGoldIncreaseUpgrade);
                break;
            case ShopItemID.xpIncreaseUpgrade:
                SetupButton(playerSave.GetXpIncreaseUpgrades(), economyData.XpIncreaseUpgradeMax, playerSave.AddXpIncreaseUpgrade);
                break;
            case ShopItemID.movementSpeedUpgrade:
                SetupButton(playerSave.GetMovementSpeedUpgrades(), economyData.MovementSpeedUpgradeMax, playerSave.AddMovementSpeedUpgrade);
                break;
        }
    }

    public void SetupFromData(ShopItemData data)
    {
        _data = data;
        SetTitleLabel(data.Title);
        SetImage(data.Image);
        SetCostLabel(data.Cost.ToString());
        _cost = data.Cost;

        SetupByID();
    }

    private void SetupButton(int current, int max, UnityAction purchaseCallback)
    {
        SetQuantityLabel(current, max);

        if (current >= max)
        {
            DisableButton();
        }
        else
        {
            _purchaseCallback = purchaseCallback;
            SetButtonCallback(OnUpgradePurchased);
        }
    }

    private void DisableButton()
    {
        _button.interactable = false;
    }

    private void SetButtonCallback(UnityAction callback)
    {
        _button.interactable = true;
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(callback);
    }

    private void OnUpgradePurchased()
    {
        if (!AttemptPurchase())
            return;
        
        _purchaseCallback();
        RefreshUi();
        SaveManager.Instance.SaveGame();
    }

    private bool AttemptPurchase()
    {
        var playerGold = SaveManager.Instance.GetPlayerSave().GetGold();
        if (playerGold < _cost)
        {
            Debug.Log("insufficient funds :C");
            return false;
        }
        
        SaveManager.Instance.GetPlayerSave().SetGold(playerGold - (int)_cost);
        return true;
    }
}
