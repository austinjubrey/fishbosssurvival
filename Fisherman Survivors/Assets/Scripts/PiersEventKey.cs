﻿using System;
using UnityEngine;

public class PiersEventKey : MonoBehaviour
{
    public enum EventKey
    {
        PlayerTakesDamage,
        PlayerGainsXP,
        PlayerLevelUp,
        EnemyKilled,
        PlayerSpawned,
        PlayerSaveUpdated,
        PlayerDied,
        PauseGame,
        LootCollected,
        GameEnded,
        TransitionToGameBegin,
        TransitionToGameComplete,
        GameStart,
        SelectedCharacterChanged
    }

    [Serializable]
    public class PiersEventEnum : SerializableEnum<EventKey> { }

    public PiersEventEnum m_EnumVariable;
}
