using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SettingsMenu : MonoBehaviour
{
    private UnityAction _exitCallback;

    public void OnExitButton()
    {
        _exitCallback();
        gameObject.SetActive(false);
    }

    public void SetCallback(UnityAction callback)
    {
        _exitCallback = callback;
    }

    public void OnResetSaveButton()
    {
        SaveManager.Instance.ResetSave();
    }
    
    public void OnGetMoneyButton()
    {
        SaveManager.Instance.GetPlayerSave().AddGold(500);
        SaveManager.Instance.SaveGame();
    }
}
