using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectController : MonoBehaviour
{
    private List<CharacterData> _characterPool;
    [SerializeField] private GameObject _characterItemPrefab;
    [SerializeField] private GameObject _content;

    public void Reinitialize()
    {
        _characterPool = DataLibrary.Instance.GetCharacterPool();
        PopulateItems();
    }

    private void PopulateItems()
    {
        GeneralUtility.UnParentAndDestroyChildren(_content.transform);
        foreach (var data in _characterPool)
        {
            CreateMenuItem(data);
        }
    }

    private void CreateMenuItem(CharacterData data)
    {
        var item = Instantiate(_characterItemPrefab, _content.transform).GetComponent<CharacterSelectItem>();
        item.SetupFromData(data);
        item.SetCallback(PopulateItems);
    }
}
