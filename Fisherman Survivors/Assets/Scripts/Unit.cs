using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    protected float _maxHealth;
    protected float _currentHealth;
    private HashSet<EStatus> _statuses = new HashSet<EStatus>();
    private HashSet<EStatus> _statusesToRemove = new HashSet<EStatus>();
    private Dictionary<EStatus, WeaponTimer> _statusTimers = new Dictionary<EStatus, WeaponTimer>();

    private float _poisonTickRate;
    
    //timers;
    private float _poisonTime = 0;

    public void ApplyStatus(EStatus status)
    {
        _statuses.Add(status);
        OnAddStatus(status);
    }

    private void OnAddStatus(EStatus status)
    {
        float duration;
        switch (status)
        {
            case EStatus.Stun:
                if (_statusTimers.TryGetValue(status, out var weaponTimer))
                {
                    weaponTimer.ResetTimer();
                }
                else
                {
                    duration = SaveManager.Instance.GetEconomyData().DefaultStunDuration;
                    _statusTimers.Add(status, new WeaponTimer(duration, OnStunExpired));
                }
                break;
            case EStatus.Poison:
                _poisonTickRate = SaveManager.Instance.GetEconomyData().PoisonTickFrequency;
                duration = SaveManager.Instance.GetEconomyData().DefaultPoisonDuration;
                _statusTimers.Add(status, new WeaponTimer(duration, OnPoisonExpired));
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(status), status, null);
        }
    }

    protected bool HasStatus(EStatus status)
    {
        return _statuses.Contains(status);
    }

    protected void Update()
    {
        ManageTimers();
        if (_statuses.Contains(EStatus.Poison))
            ManagePoison();
    }
    
    private void ManageTimers()
    {
        if (_statusTimers == null)
            return;

        foreach (var statusToRemove in _statusesToRemove)
        {
            _statusTimers.Remove(statusToRemove);
        }
        
        foreach (var entry in _statusTimers)
        {
            entry.Value?.TickCounter();
        }
    }

    private void ManagePoison()
    {
        _poisonTime += Time.deltaTime;
        
        if(_poisonTime >= _poisonTickRate)
            TakePoisonDamage();
    }

    private void RemoveStatus(EStatus status)
    {
        _statuses.Remove(status);
        _statusesToRemove.Add(status);
    }

    private void OnStunExpired()
    {
        RemoveStatus(EStatus.Stun);
    }
    
    private void OnPoisonExpired()
    {
        RemoveStatus(EStatus.Poison);
    }

    private void TakePoisonDamage()
    {
        TakeDamage(SaveManager.Instance.GetEconomyData().DefaultPoisonDamagePerTick);
        _poisonTime = 0;
    }

    protected virtual void TakeDamage(float damage)
    {
        _currentHealth -= damage;
    }
}
