using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FishbaseMenuItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Image _itemImage;
    [SerializeField] private TextMeshProUGUI _toolTipLabel;
    [SerializeField] private GameObject _toolTipObject;

    private bool _isSetup;

    public void Setup(WeaponData data)
    {
        _itemImage.sprite = data.WeaponSprite;
        _toolTipLabel.text = data.Name + " - " + data.Description;
        _isSetup = true;
    }
    
    public void Setup(ToolData data)
    {
        _itemImage.sprite = data.ToolSprite;
        _toolTipLabel.text = data.Name + " - " + data.Description;
        _isSetup = true;
    }
    
    public void Setup(EnemyData data)
    {
        SetImage(data.DisplaySprite, 25);
        _toolTipLabel.text = data.Name + " - " + data.Description;
        _isSetup = true;
    }
    
    public void Setup(CharacterData data)
    {
        _itemImage.sprite = data.MainMenuSprite;
        SetImage(data.MainMenuSprite, 10);
        _toolTipLabel.text = data.CharacterName;
        _isSetup = true;
    }

    private void SetImage(Sprite sprite, int pixels)
    {
        _itemImage.sprite = sprite;
        _itemImage.rectTransform.sizeDelta = sprite.bounds.size * pixels;
        var newBounds = _itemImage.rectTransform.sizeDelta;

        if (newBounds.x > 150 || newBounds.y > 150)
        {
            _itemImage.rectTransform.sizeDelta = new Vector2(150, 150);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _toolTipObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.position += new Vector3(0,0, -10);
        _toolTipObject.SetActive(false);
    }
}
