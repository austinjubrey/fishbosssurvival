using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawnController : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private WaveData _waveData;
    [SerializeField] public GameObject _mainEnemyPrefab;

    private float _spawnCooldown = 45f;
    private float _spawnCounter;
    private int _waveIndex = -1;
    private List<Enemy> _enemyList;
    private int _earlyWaveTimeLimit = 10;
    private float _clusterDelayMin = 0.5f;
    private float _clusterDelayMax = 2.5f;
    private bool _shouldTick;
    
    // Start is called before the first frame update
    void Start()
    {
        _waveIndex = -1;
        _spawnCounter = 0;
        _enemyList = new List<Enemy>();
        
        PiersEvent.Listen(PiersEventKey.EventKey.GameEnded, OnGameReset);
        PiersEvent.Listen(PiersEventKey.EventKey.GameStart, OnGameStart);
    }

    private void OnGameStart()
    {
        _shouldTick = true;
    }

    private void OnGameReset()
    {
        _waveIndex = -1;
        _spawnCounter = 0;
        _enemyList = new List<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_shouldTick)
            return;
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            SpawnNextWave();
        }

        ManageEarlyWaveTransition();
        TickSpawnCounter();
    }

    private void ManageEarlyWaveTransition()
    {
        if (_spawnCounter <= _spawnCooldown - _earlyWaveTimeLimit && _enemyList.Count < 2)
        {
            OnSpawnCounterExpired();
        }
    }

    private void TickSpawnCounter()
    {
        _spawnCounter -= Time.deltaTime;

        if (_spawnCounter <= 0)
        {
            OnSpawnCounterExpired();
        }
    }

    private void OnSpawnCounterExpired()
    {
        _spawnCounter = _spawnCooldown;
        SpawnNextWave();
    }

    private void SpawnNextWave()
    {
        if (_waveIndex < _waveData._waveInfo.Count - 1)
        {
            _waveIndex++;
        }

        StartCoroutine(nameof(DelayedSpawnPattern));
    }

    private IEnumerator DelayedSpawnPattern()
    {
        WaveInfo waveInfo = _waveData._waveInfo[_waveIndex];
        int numClustersToSpawn = Random.Range((int)waveInfo.ClusterSpawnRange.x, (int)waveInfo.ClusterSpawnRange.y);
        float clusterDelay = Random.Range(_clusterDelayMin, _clusterDelayMax);

        for (int i = 0; i < numClustersToSpawn; i++)
        {
            var clusterSize = Random.Range((int)waveInfo.ClusterSizeRange.x, (int)waveInfo.ClusterSizeRange.y);
            var position = GenerateRandomOffScreenLocation();
            for (int j = 0; j < clusterSize; j++)
            {
                SpawnEnemy(position);
            }
            
            yield return new WaitForSeconds(clusterDelay);
        }
    }
    
    private void SpawnEnemy(Vector3 position)
    {
        var enemy = Instantiate(_mainEnemyPrefab, position, Quaternion.identity);
        Enemy enemyScript = enemy.GetComponent<Enemy>();
        enemyScript.SetupFromData(_waveData._waveInfo[_waveIndex].EnemyData);
        enemyScript.SetTarget(_player);
        enemyScript.ApplyMultipliers(_waveData._waveInfo[_waveIndex].HealthMultiplier, _waveData._waveInfo[_waveIndex].DamageMultiplier);
        _enemyList.Add(enemyScript);
        enemyScript.SetDeathCallback(OnEnemyDeath);
    }

    private void OnEnemyDeath(Enemy deadEnemy)
    {
        _enemyList.Remove(deadEnemy);
    }

    private Vector3 GenerateRandomOffScreenLocation()
    {
        var xModifier = Random.Range(0, 2);
        var yModifier = Random.Range(0, 2);
        var randomX = Random.Range(1.1f, 1.2f);
        var randomY = Random.Range(1.1f, 1.2f);
        var shouldVaryX = Random.Range(0, 2) == 0;
        var modifier = Random.Range(0.2f, 0.8f);

        randomX = xModifier >= 0.5f ? randomX : (randomX - 1) * -1;
        randomY = yModifier >= 0.5f ? randomY : (randomY - 1) * -1;

        if (shouldVaryX)
        {
            randomX = randomX > 0 ? randomX - modifier : randomX + modifier;
        }
        else
        {
            randomY = randomY > 0 ? randomY - modifier : randomY + modifier;
        }

        return Camera.main.ViewportToWorldPoint( new Vector3(randomX, randomY, 0));
    }
}
