using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WeaponTimer
{
    private bool _isActive = false;
    private float _maxTimer;
    private float _counter;
    private UnityAction _callback;
    private UnityAction<EWeaponID> _weaponIdCallback;
    private EWeaponID _weaponID;

    public WeaponTimer(float maxTime, UnityAction callback)
    {
        _counter = 0;
        _maxTimer = maxTime;
        _callback = callback;
        _isActive = true;
    }

    public WeaponTimer(float maxTime, UnityAction<EWeaponID> callback, EWeaponID weaponID)
    {
        _counter = 0;
        _maxTimer = maxTime;
        _weaponIdCallback = callback;
        _weaponID = weaponID;
        _isActive = true;
    }

    public void SetMaxTime(float value)
    {
        _maxTimer = value;
    }
    
    public void ResetTimer()
    {
        _counter = _maxTimer;
    }

    public void TickCounter()
    {
        if (_isActive)
        {
            _counter += Time.deltaTime;

            if (_counter >= _maxTimer)
            {
                if (_callback == null && _weaponIdCallback != null)
                    _weaponIdCallback(_weaponID);
                else if(_weaponIdCallback == null)
                    _callback?.Invoke();
                _counter = 0;
            }
        }
    }
}
