using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicFishingHookProjectile : BasicProjectile
{
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private GameObject _fishingLinePoint;
    
    protected float _castTime = 3f;
    protected float _castTimeCounter;
    protected float _returnSpeed;
    protected bool _isCasting = true;
    
    //Hooking Mechanic
    protected bool _isHooking; //when true, we'll move the enemy
    protected float _hookLimit = 3f; // prevents the hook from bringing things too close
    protected List<Enemy> _hookedEnemies;
    protected bool _canHook;
    
    // Start is called before the first frame update
    void Start()
    {
        _hookedEnemies = new List<Enemy>();
        _returnSpeed = _speed * 1.5f;
        _castTime = _range / 2f;
        _collisionHelper.SetTriggerCallback(OnTriggerEnter2D);
    }

    public override void SetRange(float range)
    {
        _range = range + SaveManager.Instance.GetUpgradedFishingLineLength();
    }

    public override void SetTargetLocation(Vector3 targetLocation)
    {
        _direction = (transform.position - targetLocation).normalized;

        if (_isCasting)
        {
            float angle = Mathf.Atan2(-_direction.y, -_direction.x) * Mathf.Rad2Deg + 90;
            _spriteRenderer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public void SetCanHook(bool canHook)
    {
        _canHook = canHook;
    }

    // Update is called once per frame
    protected void Update()
    {
        ManageHook();
        ManageLine();
        ManageLifeTime();
        if(_isCasting)
            ManageCastMovement();
        else
            ManageReturnMovement();
    }

    protected void ManageHook()
    {
        if (!_isHooking)
            return;

        if (Vector3.Distance(transform.position, _owner.transform.position) <= _hookLimit)
        {
            _isHooking = false;
            DetachHookFromEnemies();
        }
    }

    protected void ManageLine()
    {
        Vector3 sp = _fishingLinePoint.transform.position;
        Vector3 ep = _owner.transform.position;
        _lineRenderer.SetVertexCount(2);
        _lineRenderer.SetPosition(0, sp);
        _lineRenderer.SetPosition(1, ep);
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
            return;
        
        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;

        enemy.OnHit(GetDamage());
        
        if (_isHooking)
        {
            enemy.AttachHook(gameObject);
            _hookedEnemies.Add(enemy);
        }
    }

    protected virtual void ManageCastMovement()
    {
        transform.position += -_direction * Time.deltaTime * _speed;
        _castTimeCounter += Time.deltaTime;

        if (_castTimeCounter >= _castTime)
        {
            OnEndCast();
        }
    }

    protected virtual void OnEndCast()
    {
        _isCasting = false;
        _isHooking = _canHook; //perhaps this isn't the default behavior
    }

    protected virtual void ManageReturnMovement()
    {
        SetTargetLocation(_owner.transform.position);
        transform.position += -_direction * Time.deltaTime * _returnSpeed;

        if (Vector3.Distance(transform.position, _owner.transform.position) <= 0.5f)
        {
            DetachHookFromEnemies();
            OnEndLifeTime();
        }
    }

    protected void DetachHookFromEnemies()
    {
        foreach (var enemy in _hookedEnemies)
        {
            if(enemy != null)
                enemy.DetachHook();
        }
    }
}
