using UnityEngine;

public class WhirlyRodProjectile : BasicFishingHookProjectile
{
    private float _maxDistance = 2;
    private bool _reachedApex;
    private float _angle;
    
    void Update()
    {
        base.Update();

        RotateSprite();
    }

    private void RotateSprite()
    {
        var direction = (_owner.transform.position - transform.position).normalized;
        float angle = Mathf.Atan2(-direction.y, -direction.x) * Mathf.Rad2Deg + 90;
        _spriteRenderer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    protected override void ManageCastMovement()
    {
        var distance = Vector3.Distance(transform.position, _owner.transform.position);
        if (!_reachedApex && distance <= _range)
        {
            transform.position += -_direction * Time.deltaTime * _speed;
        }
        else if(!_reachedApex)
        {
            _reachedApex = true;
            _angle = Vector3.AngleBetween(_owner.transform.forward, _owner.transform.position - transform.position);
        }

        if (_reachedApex)
        {
            _angle += _speed * Time.deltaTime;
            var offset = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * _range;
            transform.position = _owner.transform.position + (Vector3)offset;
        }

        _castTimeCounter += Time.deltaTime;

        if (_castTimeCounter >= _castTime)
        {
            OnEndCast();
        }
    }
    
    protected override void OnEndCast()
    {
        _isCasting = false;
        _isHooking = _canHook; //perhaps this isn't the default behavior
        DetachHookFromEnemies();
    }
    
    protected override void ManageReturnMovement()
    {
        SetTargetLocation(_owner.transform.position);
        transform.position += -_direction * Time.deltaTime * _returnSpeed;

        if (Vector3.Distance(transform.position, _owner.transform.position) <= 0.5f)
        {
            OnEndLifeTime();
        }
    }
}
