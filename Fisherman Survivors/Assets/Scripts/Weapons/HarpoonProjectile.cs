using UnityEngine;

public class HarpoonProjectile : BasicProjectile
{
    public override void SetTargetLocation(Vector3 targetLocation)
    {
        _direction = (transform.position - targetLocation).normalized;
        float angle = Mathf.Atan2(-_direction.y, -_direction.x) * Mathf.Rad2Deg - 45;
        _spriteRenderer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
            return;
        
        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;
        
        _numEnemiesHit++;
        other.gameObject.GetComponentInParent<Enemy>().OnHit(GetDamage());
        if(_numEnemiesHit >= _maxEnemiesPierce)
            OnPierceCapReached();
    }
    
    protected virtual void OnPierceCapReached()
    {
        OnEndLifeTime();
    }
}
