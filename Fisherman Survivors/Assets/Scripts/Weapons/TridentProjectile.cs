using System.Collections.Generic;
using UnityEngine;

public class TridentProjectile : HarpoonProjectile
{
    //Hooking Mechanic
    private List<Enemy> _stuckEnemies;
    private bool _canStick = true;

    new void Start()
    {
        base.Start();
        _stuckEnemies = new List<Enemy>();
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
            return;
        
        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;
        
        other.gameObject.GetComponentInParent<Enemy>().OnHit(GetDamage());
        
        if (_canStick)
        {
            enemy.AttachHook(gameObject);
            _stuckEnemies.Add(enemy);
            
            if(_stuckEnemies.Count >= _maxEnemiesPierce)
                OnPierceCapReached();
        }
    }

    protected override void OnEndLifeTime()
    {
        DetachHookFromEnemies();
        base.OnEndLifeTime();
    }
    
    private void DetachHookFromEnemies()
    {
        foreach (var enemy in _stuckEnemies)
        {
            if(enemy != null)
                enemy.DetachHook();
        }
    }

    protected override void OnPierceCapReached()
    {
        _canStick = false;
    }
}
