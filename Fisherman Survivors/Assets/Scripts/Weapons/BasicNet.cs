using UnityEngine;

public class BasicNet : MonoBehaviour
{
    [SerializeField] private BoxColliderHelper _colliderHelper;
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    private float _damage;
    private Vector3 _direction;
    private Vector3 _targetLocation;
    private bool _rotationSet;
    private bool _hasSwung;
    private bool _useBigSwing;
    
    // Start is called before the first frame update
    void Start()
    {
        if (!_rotationSet)
            RotateTowardEnemy();

        _colliderHelper.SetTriggerCallback(OnTriggerEnter2D);
    }

    public void SetDamage(float value)
    {
        _damage = value;
    }

    public void SetIsBigSwing()
    {
        _useBigSwing = true;
    }

    private void RotateTowardEnemy()
    {
        _direction = (transform.position - _targetLocation).normalized;
        float angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        _rotationSet = true;
    }

    public void SetTargetLocation(Vector3 targetLocation)
    {
        _targetLocation = targetLocation;
        RotateTowardEnemy();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
            return;

        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;
        
        other.gameObject.GetComponentInParent<Enemy>().OnHit(GetDamage());
    }

    private float GetDamage()
    {
        //other modifiers added here
        return _damage;
    }

    private void OnEndAnimation()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_hasSwung && _rotationSet)
        {
            _hasSwung = true;
            //_animator.SetTrigger(_targetLocation.x < transform.position.x ? "Swing" : "SwingRight");
            _animator.SetTrigger(_useBigSwing ? "SeaweedSwing" : "Swing");
            _animator.SetTrigger("Swing");
        }
    }
}
