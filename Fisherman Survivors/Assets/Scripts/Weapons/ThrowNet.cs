using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowNet : BasicProjectile
{
    [SerializeField] private Sprite _deployedSprite;
    private float _deployThreshold;
    private bool _isDeployed = false;
    
    public override void SetTargetLocation(Vector3 targetLocation)
    {
        _direction = (transform.position - targetLocation).normalized;
        float angle = Mathf.Atan2(-_direction.y, -_direction.x) * Mathf.Rad2Deg - 45;
        _spriteRenderer.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        CalculateDeployThreshold();
    }

    private void CalculateDeployThreshold()
    {
        _deployThreshold =  SaveManager.Instance.GetEconomyData().DefaultThrowNetDeployTime;
    }
    
    protected void Update()
    {
        ManageLifeTime();
        if(!_isDeployed)
            transform.position += -_direction * Time.deltaTime * _speed;
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (_currentLifeTime < _deployThreshold)
            return;
        
        if (other == null)
            return;
        
        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;
        
        if(!_isDeployed)
            OnDeployed();
        
        var enemyScript = other.gameObject.GetComponentInParent<Enemy>();
        enemyScript.OnHit(GetDamage());
        enemyScript.ApplyStatus(EStatus.Stun);
        
    }

    private void OnDeployed()
    {
        _isDeployed = true;
        _spriteRenderer.sprite = _deployedSprite;
        StartCoroutine(nameof(DelayedDestroy));
    }

    private IEnumerator DelayedDestroy()
    {
        yield return new WaitForSeconds(0.5f);
        
        OnEndLifeTime();
    }
}
