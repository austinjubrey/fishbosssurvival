using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]private Player _player;
    private Camera _camera;
    private float _speed;
    private bool _canMove = true;
    
    // Start is called before the first frame update
    void Start()
    {
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerDied, OnPlayerDied);
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerSpawned, OnCharacterSetup);
    }

    // Update is called once per frame
    void Update()
    {
        if(_camera == null)
            return;

        Move();
    }

    private void OnCharacterSetup()
    {
        _camera = gameObject.GetComponent<Camera>();
        _speed = _player.GetSpeed();
    }

    private void OnPlayerDied()
    {
        _canMove = false;
    }

    private void Move()
    {
        if (!_canMove)
            return;
        
        var horizontalMovement = Input.GetAxis("Horizontal");
        var verticalMovement = Input.GetAxis("Vertical");

        transform.position = new Vector3(transform.position.x + horizontalMovement * _speed * Time.deltaTime,
            transform.position.y + verticalMovement * _speed * Time.deltaTime, transform.position.z);
    }
}
