using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class Player : Unit
{
    private static Vector3 _errorVector = new Vector3(-100, -100, -100);

    private float _currentXP;
    private int _level = 1;
    
    private float _moveSpeed;
    private Dictionary<EWeaponID, WeaponTimer> _weaponTimers;

    [SerializeField] private BoxColliderHelper _collisionHelper;
    [SerializeField] private AnimationHandler _animationHandler;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private ExperienceData _experienceData;
    private CharacterData _characterData;

    private List<EquippedWeaponInfo> _equippedWeapons;
    private List<EquippedToolInfo> _equippedTools;
    [SerializeField] private int _maxWeapons;
    [SerializeField] private int _maxTools;

    private bool _gameStarted;
    private float _dropTime;
    
    private bool _isFacingLeft = true;
    private bool _isMoving;
    private float _movingAnimationSpeed = 0.25f;
    private float _idleAnimationSpeed = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        _weaponTimers = new Dictionary<EWeaponID, WeaponTimer>();
        _equippedWeapons = new List<EquippedWeaponInfo>();
        _equippedTools = new List<EquippedToolInfo>();
        
        SetupCharacter();
        
        _animationHandler.SetFrameSpeed(_idleAnimationSpeed);
        _maxHealth = CalculateMaxHealth();
        _currentHealth = _maxHealth;
        _moveSpeed = CalculateMovementSpeed();

        PiersEvent.Listen<EEnemyID>(PiersEventKey.EventKey.EnemyKilled, OnEnemyKilled);
        PiersEvent.Listen(PiersEventKey.EventKey.GameStart, OnGameStart);
        PiersEvent.Listen(PiersEventKey.EventKey.TransitionToGameComplete, DropInTween);
    }

    private void DropInTween()
    {
        LeanTween.move(gameObject, new Vector2(0, 0), 2).setOnComplete(OnPlayerReady).setEase(LeanTweenType.easeOutCubic);
    }

    private void OnPlayerReady()
    {
        PiersEvent.Post(PiersEventKey.EventKey.PlayerSpawned);
    }

    // Update is called once per frame
    void Update()
    {
        if(!_gameStarted)
            return;
        
        base.Update();
        
        if (_currentHealth > 0)
        {
            Move();
            AdvanceTimers();
        }
        
        if(Input.GetKeyDown(KeyCode.L))
            LevelUp();
    }

    private void SetupCharacter()
    {
        var characterID = SaveManager.Instance.GetPlayerSave().GetSelectedCharacterID();
        _characterData = DataLibrary.Instance.GetCharacterDataByID(characterID);
        _animationHandler.SetTexture(_characterData.SwimmingSprites);
        var WeaponData = DataLibrary.Instance.GetWeaponDataByID(_characterData.DefaultWeaponID);
        EquipWeapon(WeaponData);
    }

    private float CalculateMaxHealth()
    {
        var characterBonus = _characterData.CharacterBaseHealth;
        var bonus = SaveManager.Instance.GetUpgradedHealthValue() * 0.01f;
        var bonusValue = bonus * characterBonus;
        var total = characterBonus + bonusValue;
        return total;
    }
    
    private float CalculateMovementSpeed()
    {
        var characterBonus = _characterData.CharacterMovementSpeed;
        var bonus = SaveManager.Instance.GetUpgradedMovementSpeedValue() * 0.01f;
        var bonusValue = bonus * characterBonus;
        var total = characterBonus + bonusValue;
        return total;
    }

    private void OnEnemyKilled(EEnemyID ID)
    {
        GainXP(10);
    }

    private void GainXP(float amount)
    {
        var bonus = SaveManager.Instance.GetUpgradedXpIncreaseValue() * 0.01f;
        var bonusValue = bonus * amount;
        _currentXP += amount + bonusValue;
        PiersEvent.Post(PiersEventKey.EventKey.PlayerGainsXP);

        if (_currentXP >= GetMaxXP())
        {
            LevelUp();
        }
    }

    public float GetMaxXP()
    {
        return _level - 1 < _experienceData._experienceThresholds.Count ? _experienceData._experienceThresholds[_level - 1] : _experienceData._experienceThresholds[^1];
    }

    public int GetLevel()
    {
        return _level;
    }

    private void LevelUp(float extraXP = 0)
    {
        _level++;
        _currentXP = extraXP;
        PiersEvent.Post(PiersEventKey.EventKey.PlayerGainsXP);
        PiersEvent.Post(PiersEventKey.EventKey.PlayerLevelUp);
    }

    public void LevelUpWeapon(EWeaponID ID)
    {
        GetWeaponByWeaponID(ID).LevelUp();
        var info = _equippedWeapons.Find(x=>x.WeaponID == ID);
        _weaponTimers[ID].SetMaxTime(CalculateWeaponFireRate(info));
    }
    
    public void LevelUpTool(EToolID ID)
    {
        GetToolByID(ID).LevelUp();
        UpdateWeaponValues();
    }

    private void UpdateWeaponValues()
    {
        foreach (var weaponInfo in _equippedWeapons)
        {
            _weaponTimers[weaponInfo.WeaponID].SetMaxTime(CalculateWeaponFireRate(weaponInfo));
        }
    }

    private void OnGameStart()
    {
        _gameStarted = true;
    }

    public List<EquippedWeaponInfo> GetEquippedWeaponInfo()
    {
        return _equippedWeapons;
    }
    
    public List<EquippedToolInfo> GetEquippedToolInfo()
    {
        return _equippedTools;
    }

    private EquippedWeaponInfo GetWeaponByWeaponID(EWeaponID weaponID)
    {
        return _equippedWeapons.FirstOrDefault(equippedWeaponInfo => equippedWeaponInfo.WeaponID.Equals(weaponID));
    }
    
    private EquippedToolInfo GetToolByID(EToolID toolID)
    {
        return _equippedTools.FirstOrDefault(equippedToolInfo => equippedToolInfo.ToolID.Equals(toolID));
    }

    public void EquipWeapon(WeaponData data)
    {
        var info = new EquippedWeaponInfo();
        info.SetData(data);
        _equippedWeapons.Add(info);
        _weaponTimers.Add(data.WeaponID, new WeaponTimer(info.FireRate, FireWeapon, data.WeaponID));
    }
    
    public void EquipTool(ToolData data)
    {
        var info = new EquippedToolInfo();
        info.SetData(data);
        _equippedTools.Add(info);
        UpdateWeaponValues();
    }
    
    public bool GetHasToolByID(EToolID id)
    {
        return _equippedTools.Any(weapon => weapon.ToolID.Equals(id));
    }

    public bool GetHasWeaponByID(EWeaponID id)
    {
        return _equippedWeapons.Any(weapon => weapon.WeaponID.Equals(id));
    }

    public bool HasAvailableWeaponUpgrades()
    {
        return _equippedWeapons.Any(weaponInfo => weaponInfo.Level - 1 < weaponInfo.MaxLevel);
    }
    
    public bool HasAvailableToolUpgrades()
    {
        return _equippedTools.Any(toolInfo => toolInfo.Level - 1 < toolInfo.MaxLevel);
    }

    public bool HasMaxNumWeapons()
    {
        return _equippedWeapons.Count == _maxWeapons;
    }
    
    public bool HasMaxTools()
    {
        return _equippedTools.Count == _maxTools;
    }

    public float GetSpeed()
    {
        return _moveSpeed;
    }

    public float GetHealth()
    {
        return _currentHealth;
    }

    public float GetCurrentXP()
    {
        return _currentXP;
    }

    public float GetMaxHealth()
    {
        return _maxHealth;
    }

    public int GetNumEquippedWeapons()
    {
        return _equippedWeapons.Count;
    }
    
    public int GetNumEquippedTools()
    {
        return _equippedTools.Count;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            TakeDamage(other.gameObject.GetComponent<Enemy>().GetMeleeDamage());
        }
    }

    protected override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        PiersEvent.Post(PiersEventKey.EventKey.PlayerTakesDamage);

        if (_currentHealth <= 0)
        {
            //player death
            OnPlayerDeath();
        }
    }

    private void OnPlayerDeath()
    {
        PiersEvent.Post(PiersEventKey.EventKey.PlayerDied);
    }

    private void AdvanceTimers()
    {
        if (_weaponTimers == null)
            return;
        
        foreach (var entry in _weaponTimers)
        {
            entry.Value?.TickCounter();
        }
    }

    private void Move()
    {
        var horizontalMovement = Input.GetAxis("Horizontal");
        var verticalMovement = Input.GetAxis("Vertical");

        ManageSpriteDirection(horizontalMovement);
        ManageAnimationSpeed(horizontalMovement, verticalMovement);

        transform.position = new Vector3(transform.position.x + horizontalMovement * _moveSpeed * Time.deltaTime,
            transform.position.y + verticalMovement * _moveSpeed * Time.deltaTime, transform.position.z);
    }

    private void ManageAnimationSpeed(float horizontal, float vertical)
    {
        if (_isMoving && horizontal == 0 && vertical == 0)
        {
            _isMoving = false;
            _animationHandler.SetFrameSpeed(_idleAnimationSpeed);
        }
        else if (!_isMoving && horizontal != 0 || vertical != 0)
        {
            _isMoving = true;
            _animationHandler.SetFrameSpeed(_movingAnimationSpeed);
        }
    }

    private void ManageSpriteDirection(float horizontalMovement)
    {
        if (_isFacingLeft && horizontalMovement > 0)
        {
            _spriteRenderer.flipX = true;
            _isFacingLeft = false;
        }
        else if (!_isFacingLeft && horizontalMovement < 0)
        {
            _spriteRenderer.flipX = false;
            _isFacingLeft = true;
        }
    }

    private float CalculateProjectileDamage(EquippedWeaponInfo weaponInfo)
    {
        var weaponDamage = weaponInfo.Damage;
        var totalToolDamage = CalculateTotalToolStat(_equippedTools, EUpgradeType.Damage);
        var characterBonus = _characterData.CharacterDamage;

        var combinedDamage = weaponDamage + totalToolDamage + characterBonus;
        var bonus = SaveManager.Instance.GetUpgradedDamageValue() * 0.01f;
        var bonusValue = bonus * combinedDamage;
        var total = combinedDamage + bonusValue;

        return total;
    }
    
    private float CalculateProjectileSizeModifier(EquippedWeaponInfo weaponInfo)
    {
        var weaponSizeModifier = weaponInfo.SizeModifier;
        var totalToolSizeModifier = CalculateTotalToolStat(_equippedTools, EUpgradeType.SizeModifier);
        var characterBonus = _characterData.CharacterSizeModifier;

        return 1 + weaponSizeModifier + totalToolSizeModifier + characterBonus;
    }
    
    private int CalculateProjectilePierce(EquippedWeaponInfo weaponInfo)
    {
        var weaponPierce = weaponInfo.EnemyPierce;
        var totalToolPierce = (int)CalculateTotalToolStat(_equippedTools, EUpgradeType.ExtraPierce);
        var characterBonus = _characterData.CharacterEnemyPierce;

        return weaponPierce + totalToolPierce + characterBonus;
    }
    
    private float CalculateWeaponFireRate(EquippedWeaponInfo weaponInfo)
    {
        var weaponFireRate = weaponInfo.FireRate;
        var totalToolFireRate = CalculateTotalToolStat(_equippedTools, EUpgradeType.FireRate);
        var characterBonus = _characterData.CharacterFireRate;

        return weaponFireRate + totalToolFireRate + characterBonus;
    }
    
    private int CalculateWeaponProjectiles(EquippedWeaponInfo weaponInfo)
    {
        var weaponExtraProjectiles = weaponInfo.ExtraProjectiles;
        var totalToolExtraProjectiles = (int)CalculateTotalToolStat(_equippedTools, EUpgradeType.ExtraProjectiles);
        var characterBonus = _characterData.CharacterExtraProjectiles;

        return 1 + weaponExtraProjectiles + totalToolExtraProjectiles + characterBonus;
    }
    
    private float CalculateWeaponRange(EquippedWeaponInfo weaponInfo)
    {
        var weaponRange = weaponInfo.Range;
        var totalToolRange = CalculateTotalToolStat(_equippedTools, EUpgradeType.Range);
        var characterBonus = _characterData.CharacterRange;

        return weaponRange + totalToolRange + characterBonus;
    }

    private float CalculateTotalToolStat(List<EquippedToolInfo> infoList, EUpgradeType stat)
    {
        float total = 0;
        float modifier = 1;

        if (stat == EUpgradeType.FireRate)
            modifier = EquippedBattleItemInfo.FIRE_RATE_FLOOR;
        foreach (var equippedToolInfo in infoList)
        {
            total += stat switch
            {
                EUpgradeType.Damage => equippedToolInfo.Damage,
                EUpgradeType.FireRate => equippedToolInfo.FireRate,
                EUpgradeType.Range => equippedToolInfo.Range,
                EUpgradeType.SizeModifier => equippedToolInfo.SizeModifier,
                EUpgradeType.ExtraProjectiles => equippedToolInfo.ExtraProjectiles,
                EUpgradeType.ExtraPierce => equippedToolInfo.EnemyPierce,
                _ => throw new ArgumentOutOfRangeException(nameof(stat), stat, null)
            };
            total += WeaponDataHelper.GetBattleItemStatAtLevel(DataLibrary.Instance.GetToolDataByID(equippedToolInfo.ToolID), equippedToolInfo.Level - 1, stat) * modifier;
        }

        return total;
    }

    #region Targeting

    private Vector3 GetRandomEnemyPosition()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        int randomIndex = Random.Range(0, enemies.Length);
        
        if(enemies.Length == 0 || randomIndex > enemies.Length - 1)
            return _errorVector;

        var enemy = enemies[randomIndex];

        return enemy != null ? enemy.transform.position : _errorVector;
    }

    private Vector3 GetClosestEnemyPosition()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies.Length == 0)
            return _errorVector;
        
        Vector3 closestPosition = enemies[0].transform.position;
        float closestDistance = Vector3.Distance(transform.position, closestPosition);
        foreach (var enemy in enemies)
        {
            var distance = Vector3.Distance(transform.position, enemy.transform.position);
            if (distance < closestDistance)
            {
                closestPosition = enemy.transform.position;
                closestDistance = distance;
            }
        }

        return closestPosition;
    }

    private Vector3 GetFirstEnemyPosition()
    {
        var enemy = GameObject.FindGameObjectWithTag("Enemy");

        return enemy != null ? enemy.transform.position : _errorVector;
    }
    
    #endregion
    
    #region Attacks

    private void FireWeapon(EWeaponID weaponID)
    {
        WeaponData weaponData = DataLibrary.Instance.GetWeaponDataByID(weaponID);
        var equippedWeaponInfo = GetWeaponByWeaponID(weaponID);
        int numProjectiles = CalculateWeaponProjectiles(equippedWeaponInfo);

        for (int i = 0; i < numProjectiles; i++)
        {
            var randomEnemyPosition = GetTargetByWeaponID(weaponID, i);
            if (randomEnemyPosition == _errorVector) //failed to find a target
                return;
            
            var instance = Instantiate(weaponData.ProjectilePrefab, transform);
            Physics2D.IgnoreCollision(instance.GetComponentInChildren<BoxCollider2D>(), GetComponentInChildren<BoxCollider2D>());
            instance.transform.SetParent(null);

            SetupProjectileByID(instance, randomEnemyPosition, weaponID);
        }
    }

    //should include every type of weapon
    private void SetupProjectileByID(GameObject gameObject, Vector3 target, EWeaponID weaponID)
    {
        var equippedWeaponInfo = GetWeaponByWeaponID(weaponID);
        
        switch (weaponID)
        {
            case EWeaponID.FishingRod:
                var fishingHookProjectile = gameObject.GetComponent<BasicFishingHookProjectile>();
                fishingHookProjectile.SetTargetLocation(target);
                fishingHookProjectile.SetOwner(this);
                fishingHookProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                fishingHookProjectile.SetRange(CalculateWeaponRange(equippedWeaponInfo));
                fishingHookProjectile.SetCanHook(SaveManager.Instance.GetPlayerSave().GetHasUnlockedHook());
                fishingHookProjectile.SetScale(CalculateProjectileSizeModifier(equippedWeaponInfo));
                break;
            case EWeaponID.Harpoon:
                var harpoonProjectile = gameObject.GetComponent<HarpoonProjectile>();
                harpoonProjectile.SetTargetLocation(target);
                harpoonProjectile.SetOwner(this);
                harpoonProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                harpoonProjectile.SetEnemyPierce(CalculateProjectilePierce(equippedWeaponInfo));
                harpoonProjectile.SetScale(CalculateProjectileSizeModifier(equippedWeaponInfo));
                harpoonProjectile.SetRange(CalculateWeaponRange(equippedWeaponInfo));
                break;
            case EWeaponID.Net:
                gameObject.transform.SetParent(transform);
                var basicNetProjectile = gameObject.GetComponent<BasicNet>();
                basicNetProjectile.SetTargetLocation(target);
                basicNetProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                break;
            case EWeaponID.Trident:
                var tridentProjectile = gameObject.GetComponent<TridentProjectile>();
                tridentProjectile.SetTargetLocation(target);
                tridentProjectile.SetOwner(this);
                tridentProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                tridentProjectile.SetRange(CalculateWeaponRange(equippedWeaponInfo));
                tridentProjectile.SetEnemyPierce(CalculateProjectilePierce(equippedWeaponInfo));
                tridentProjectile.SetScale(CalculateProjectileSizeModifier(equippedWeaponInfo));
                break;
            case EWeaponID.WhirlyRod:
                var whirlyRodProjectile = gameObject.GetComponent<WhirlyRodProjectile>();
                whirlyRodProjectile.SetTargetLocation(target);
                whirlyRodProjectile.SetOwner(this);
                whirlyRodProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                whirlyRodProjectile.SetRange(CalculateWeaponRange(equippedWeaponInfo));
                whirlyRodProjectile.SetCanHook(SaveManager.Instance.GetPlayerSave().GetHasUnlockedHook()); //this could be a different upgrade
                whirlyRodProjectile.SetScale(CalculateProjectileSizeModifier(equippedWeaponInfo));
                break;
            case EWeaponID.ThrowNet:
                var throwNetProjectile = gameObject.GetComponent<ThrowNet>();
                throwNetProjectile.SetTargetLocation(target);
                throwNetProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                break;
            case EWeaponID.SeaweedSword:
                gameObject.transform.SetParent(transform);
                var seaweedSwordProjectile = gameObject.GetComponent<BasicNet>();
                seaweedSwordProjectile.SetTargetLocation(target);
                seaweedSwordProjectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                seaweedSwordProjectile.SetIsBigSwing();
                break;
            case EWeaponID.Anchor:
            default:
                var projectile = gameObject.GetComponent<BasicProjectile>();
                projectile.SetTargetLocation(target);
                projectile.SetOwner(this);
                projectile.SetDamage(CalculateProjectileDamage(equippedWeaponInfo));
                projectile.SetRange(CalculateWeaponRange(equippedWeaponInfo));
                projectile.SetScale(CalculateProjectileSizeModifier(equippedWeaponInfo));
                break;
        }
    }

    private Vector3 GetTargetByWeaponID(EWeaponID ID, int numProjectile)
    {
        switch (ID)
        {
            case EWeaponID.ThrowNet:
            case EWeaponID.Trident:
                return numProjectile == 0 ? GetClosestEnemyPosition() : GetRandomEnemyPosition();
            default:
                return GetRandomEnemyPosition();
        }
    }

    #endregion Attacks
}
