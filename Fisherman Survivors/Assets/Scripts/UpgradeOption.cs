using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UpgradeOption : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameLabel;
    [SerializeField] private TextMeshProUGUI _descLabel;
    [SerializeField] private TextMeshProUGUI _optionDescriptorLabel;
    [SerializeField] private Image _weaponImage;

    private BattleItemData _data;
    private UnityAction<levelUpChoiceInfo> _callback;
    private bool _isWeaponChoice;

    public void Setup(BattleItemData data, UnityAction<levelUpChoiceInfo> callback, bool isWeapon)
    {
        _callback = callback;
        _data = data;
        _nameLabel.text = data.Name;
        _descLabel.text = data.Description;
        _isWeaponChoice = isWeapon;
        _weaponImage.sprite = GetDataImage();
    }

    private Sprite GetDataImage()
    {
        if (_isWeaponChoice)
        {
            var weaponData = _data as WeaponData;
            return weaponData.WeaponSprite;
        }

        var toolData = _data as ToolData;
        return toolData.ToolSprite;
    }

    public void SetDescriptorText(bool isUpgrade)
    {
        if (isUpgrade)
            _optionDescriptorLabel.text = "Upgrade";
        else
            _optionDescriptorLabel.text = _isWeaponChoice ? "New Weapon" :  "New Tool";
    }

    public void OnSelectButtonPressed()
    {
        _callback?.Invoke(new levelUpChoiceInfo(_data, _isWeaponChoice));
    }
}
