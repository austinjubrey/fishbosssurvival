using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveTransitionController : MonoBehaviour
{
    [SerializeField] private GameObject _wavePrefab;
    [SerializeField] private Transform _content;
    
    private static string GAME_SCENE_NAME = "GameScene";
    private static string MAIN_MENU_SCENE = "MainMenu";
    private static string TRANSITION_SCENE = "TransitionScene";
    
    public float _bobAmplitude;
    public float _bobPeriod;
    
    public float _tiltSpeed;
    public float _tiltLimit;
    public int _numWaves;
    public float _waveSpeed;
    public float _waveDelay;
    
    // Start is called before the first frame update
    void Start()
    {
        StartTransition();
    }

    private void StartTransition()
    {
        PiersEvent.Post<float>(PiersEventKey.EventKey.TransitionToGameBegin, _waveSpeed);
        StartCoroutine(nameof(WaveAvalanche));
    }

    private IEnumerator WaveAvalanche()
    {
        int waveSpriteIndex = 0;
        for (int i = 0; i < _numWaves; i++)
        {
            var newWave = Instantiate(_wavePrefab, _content);
            Floater floater = newWave.GetComponent<Floater>();
            TransitionWave transitionWave = newWave.GetComponent<TransitionWave>();
            floater._bobAmplitude = _bobAmplitude;
            floater._bobPeriod = _bobPeriod;
            floater._tiltLimit = _tiltLimit;
            floater._tiltSpeed = _tiltSpeed;
            transitionWave.Speed = _waveSpeed;
            transitionWave.spriteIndex = waveSpriteIndex;
            waveSpriteIndex++;
            if (waveSpriteIndex >= 3)
                waveSpriteIndex = 0;

            if (i == _numWaves - 1)
                transitionWave.Callback = OnFinalWaveCleared;

            yield return new WaitForSeconds(_waveDelay);
        }

        SceneManager.LoadSceneAsync(GAME_SCENE_NAME, LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync(MAIN_MENU_SCENE);
    }

    private void OnFinalWaveCleared()
    {
        PiersEvent.Post(PiersEventKey.EventKey.TransitionToGameComplete);
        SceneManager.UnloadSceneAsync(TRANSITION_SCENE);
    }
}
