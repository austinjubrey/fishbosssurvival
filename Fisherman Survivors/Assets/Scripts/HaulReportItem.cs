using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HaulReportItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _amountLabel;
    [SerializeField] private TextMeshProUGUI _totalLabel;
    [SerializeField] private Image _image;

    public void SetAmountLabel(int amount)
    {
        _amountLabel.text = "X " + amount;
    }
    
    public void SetTotalLabel(int amount)
    {
        _totalLabel.text = "= " + amount;
    }
    
    public void SetImage(Sprite sprite)
    {
        _image.sprite = sprite;
    }
}
