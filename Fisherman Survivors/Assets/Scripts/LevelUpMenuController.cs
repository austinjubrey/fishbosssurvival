using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class levelUpChoiceInfo
{
    public BattleItemData _itemData;
    public bool _isWeapon; //if false, it's a tool

    public levelUpChoiceInfo(BattleItemData itemData, bool isWeapon)
    {
        _itemData = itemData;
        _isWeapon = isWeapon;
    }
}

public class LevelUpMenuController : MonoBehaviour
{
    [SerializeField] private GameObject _upgradeOptionPrefab;
    [SerializeField] private GameObject _optionContent;

    private Player _player;
    private int _numItems = 2; //maybe this value changes, maybe not
    
    private List<EWeaponID> _selectedUnequippedWeapons;
    private List<EToolID> _selectedUnequippedTools;
    private List<EWeaponID> _selectedWeaponUpgrades;
    private List<EToolID> _selectedToolUpgrades;

    private UnityAction _callback;

    public void Setup( UnityAction callback) //and also the data
    {
        _callback = callback;
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        SetupOptions();
    }

    private void SetupOptions()
    {
        GeneralUtility.UnParentAndDestroyChildren(_optionContent.transform);
        SetupMixedOptions();
    }

    private void SetupMixedOptions()
    {
        _selectedUnequippedWeapons = new List<EWeaponID>();
        _selectedUnequippedTools = new List<EToolID>();
        _selectedWeaponUpgrades = new List<EWeaponID>();
        _selectedToolUpgrades = new List<EToolID>();
        
        List<bool> slotDesignationList = new List<bool>();
        for (int i = 0; i < _numItems; i++)
        {
            slotDesignationList.Add(Random.Range(0,2) == 0);
        }

        foreach (var giveItemPriority in slotDesignationList)
        {
            if (giveItemPriority)
            {
                if (Random.Range(0, 2) == 0) //weapon priority
                {
                    if(!TryAddNewWeaponChoice())
                        if(!TryAddNewToolChoice())
                            if (!TryAddWeaponUpgradeChoice())
                                TryAddToolUpgradeChoice();
                }
                else //tool priority
                {
                    if(!TryAddNewToolChoice())
                        if(!TryAddNewWeaponChoice())
                            if (!TryAddToolUpgradeChoice())
                                TryAddWeaponUpgradeChoice();
                }
            }
            else //upgrade priority
            {
                if (Random.Range(0, 2) == 0) // Weapon Priority
                {
                    if(!TryAddWeaponUpgradeChoice())
                        if(!TryAddToolUpgradeChoice())
                            if (!TryAddNewWeaponChoice())
                                TryAddNewToolChoice();
                }
                else //Tool Priority
                {
                    if(!TryAddToolUpgradeChoice())
                        if(!TryAddWeaponUpgradeChoice())
                            if (!TryAddNewToolChoice())
                                TryAddNewWeaponChoice();
                }
            }
        }
    }

    private bool TryAddNewWeaponChoice()
    {
        if (_player.HasMaxNumWeapons())
            return false;
        
        WeaponData randomUnequippedWeapon = GetRandomUnEquippedWeapon(_selectedUnequippedWeapons);
        if (randomUnequippedWeapon == null)
            return false;
        
        AddSingleBattleItemOption(randomUnequippedWeapon, true);
        _selectedUnequippedWeapons.Add(randomUnequippedWeapon.WeaponID);
        return true;
    }

    private bool TryAddNewToolChoice()
    {
        if (_player.HasMaxTools())
            return false;
        
        ToolData randomUnequippedTool = GetRandomUnEquippedTool(_selectedUnequippedTools);
        if (randomUnequippedTool == null)
            return false;
        
        AddSingleBattleItemOption(randomUnequippedTool, false);
        _selectedUnequippedTools.Add(randomUnequippedTool.ToolID);
        return true;
    }

    private bool TryAddToolUpgradeChoice()
    {
        if (!_player.HasAvailableToolUpgrades())
            return false;

        var equippedToolInfo = GetRandomToolUpgrade(_selectedToolUpgrades);
        if (equippedToolInfo == null)
            return false;
        
        AddSingleUpgradeOption(equippedToolInfo, false);
        _selectedToolUpgrades.Add(equippedToolInfo.ToolID);
        return true;
    }

    private bool TryAddWeaponUpgradeChoice()
    {
        if (!_player.HasAvailableWeaponUpgrades())
            return false;

        var equippedWeaponInfo = GetRandomWeaponUpgrade(_selectedWeaponUpgrades);
        if (equippedWeaponInfo == null)
            return false;
        
        AddSingleUpgradeOption(equippedWeaponInfo, true);
        _selectedWeaponUpgrades.Add(equippedWeaponInfo.WeaponID);
        return true;
    }

    private void AddSingleBattleItemOption(BattleItemData data, bool isWeapon)
    {
        if(data == null)
            return;
        
        var element = Instantiate(_upgradeOptionPrefab, _optionContent.transform);
        element.GetComponent<UpgradeOption>().Setup(data, OnSelectionMade, isWeapon);
        element.GetComponent<UpgradeOption>().SetDescriptorText(false);
    }
    
    private void AddSingleUpgradeOption(EquippedBattleItemInfo data, bool isWeapon)
    {
        if(data == null)
            return;
        
        var element = Instantiate(_upgradeOptionPrefab, _optionContent.transform);

        if (isWeapon)
        {
            var weaponInfo = data as EquippedWeaponInfo;
            element.GetComponent<UpgradeOption>().Setup(DataLibrary.Instance.GetWeaponDataByID(weaponInfo.WeaponID), OnSelectionMade, isWeapon);
        }
        else
        {
            var toolInfo = data as EquippedToolInfo;
            element.GetComponent<UpgradeOption>().Setup(DataLibrary.Instance.GetToolDataByID(toolInfo.ToolID), OnSelectionMade, isWeapon);
        }

        element.GetComponent<UpgradeOption>().SetDescriptorText(true);
    }

    private void OnSelectionMade(levelUpChoiceInfo choiceInfo)
    {
        if (choiceInfo._isWeapon)
        {
            WeaponData weaponData = choiceInfo._itemData as WeaponData;
            if(_player.GetHasWeaponByID(weaponData.WeaponID))
                _player.LevelUpWeapon(weaponData.WeaponID);
            else
                _player.EquipWeapon(weaponData);
        }
        else
        {
            ToolData toolData = choiceInfo._itemData as ToolData;
            if(_player.GetHasToolByID(toolData.ToolID))
                _player.LevelUpTool(toolData.ToolID);
            else
                _player.EquipTool(toolData);
        }

        gameObject.SetActive(false);
        _callback();
    }

    private EquippedWeaponInfo GetRandomWeaponUpgrade(List<EWeaponID> usedWeapons)
    {
        int equippedWeaponCount = _player.GetNumEquippedWeapons();
        
        List<int> shuffledIndices = new List<int>();
        for (int i = 0; i < equippedWeaponCount; i++)
        {
            shuffledIndices.Add(i);
        }

        foreach (var index in shuffledIndices)
        {
            var randomItem= _player.GetEquippedWeaponInfo()[index];
            
            if(usedWeapons.Contains(randomItem.WeaponID))
                continue;
            
            if (randomItem.Level - 1 < randomItem.MaxLevel)
                return randomItem;
        }

        return null; //everything was max level

    }
    
    private EquippedToolInfo GetRandomToolUpgrade(List<EToolID> usedTools)
    {
        int equippedToolCount = _player.GetNumEquippedTools();
        
        List<int> shuffledIndices = new List<int>();
        for (int i = 0; i < equippedToolCount; i++)
        {
            shuffledIndices.Add(i);
        }

        foreach (var index in shuffledIndices)
        {
            var randomItem= _player.GetEquippedToolInfo()[index];

            if (usedTools.Contains(randomItem.ToolID))
                continue;
            
            if (randomItem.Level - 1 < randomItem.MaxLevel)
                return randomItem;
        }

        return null; //everything was max level

    }

    private WeaponData GetRandomUnEquippedWeapon(List<EWeaponID> usedWeapons)
    {
        var weaponDataPool = DataLibrary.Instance.GetWeaponPool();
        List<int> shuffledIndices = new List<int>();
        for (int i = 0; i < weaponDataPool.Count; i++)
        {
            shuffledIndices.Add(i);
        }

        shuffledIndices = shuffledIndices.OrderBy(i => Guid.NewGuid()).ToList();

        foreach (var index in shuffledIndices)
        {
            var weaponData = weaponDataPool[index];
            
            if(usedWeapons.Contains(weaponData.WeaponID))
            {
                continue;
            }
            
            bool isEquipped = _player.GetEquippedWeaponInfo().Any(equippedWeaponInfo => equippedWeaponInfo.WeaponID.Equals(weaponData.WeaponID));

            if (!isEquipped)
                return weaponData;
        }

        return null; //no valid options
    }
    
    private ToolData GetRandomUnEquippedTool(List<EToolID> usedTools)
    {
        var toolDataPool = DataLibrary.Instance.GetToolPool();
        List<int> shuffledIndices = new List<int>();
        for (int i = 0; i < toolDataPool.Count; i++)
        {
            shuffledIndices.Add(i);
        }

        shuffledIndices = shuffledIndices.OrderBy(i => Guid.NewGuid()).ToList();

        foreach (var index in shuffledIndices)
        {
            var toolData = toolDataPool[index];
            
            if(usedTools.Contains(toolData.ToolID))
            {
                continue;
            }
            
            bool isEquipped = _player.GetEquippedToolInfo().Any(equippedToolInfo => equippedToolInfo.ToolID.Equals(toolData.ToolID));

            if (!isEquipped)
                return toolData;
        }

        return null; //no valid options
    }
}
