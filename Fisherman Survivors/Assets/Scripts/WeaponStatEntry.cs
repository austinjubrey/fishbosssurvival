using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponStatEntry : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _weaponNameLabel;
    [SerializeField] private TextMeshProUGUI _levelLabel;
    [SerializeField] private Image _weaponImage;

    public void SetData(EquippedWeaponInfo data)
    {
        SetNameLabel(data.Name);
        SetLevelLabel(data.Level.ToString());
        SetWeaponImage(DataLibrary.Instance.GetWeaponDataByID(data.WeaponID).WeaponSprite);
    }
    
    public void SetData(EquippedToolInfo data)
    {
        SetNameLabel(data.Name);
        SetLevelLabel(data.Level.ToString());
        SetWeaponImage(DataLibrary.Instance.GetToolDataByID(data.ToolID).ToolSprite);
    }

    private void SetNameLabel(string text)
    {
        _weaponNameLabel.text = text;
    }
    
    private void SetLevelLabel(string text)
    {
        _levelLabel.text = text;
    }

    private void SetWeaponImage(Sprite sprite)
    {
        _weaponImage.sprite = sprite;
    }
}
