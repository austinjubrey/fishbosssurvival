using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelUpUpgrades
{
    public List<EUpgradeType> _upgrades = new List<EUpgradeType>();
}

[System.Serializable]
public class LevelUpTier
{
    public List<LevelUpUpgrades> _upgradeTiers = new List<LevelUpUpgrades>();
}