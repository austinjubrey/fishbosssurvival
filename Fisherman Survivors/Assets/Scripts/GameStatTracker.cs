using System.Collections.Generic;
using UnityEngine;

public class GameStatTracker : MonoBehaviour
{
    private Dictionary<EEnemyID, int> _enemyDeathLog;

    private int _goldEarned;
    private int _goldLootDropsCollected;
    
    //Singleton things
    private static GameStatTracker _instance;
    public static GameStatTracker Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _goldLootDropsCollected = 0;
        _enemyDeathLog = new Dictionary<EEnemyID, int>();
        Listen();
    }

    private void Listen()
    {
        PiersEvent.Listen<EEnemyID>(PiersEventKey.EventKey.EnemyKilled, OnEnemyDied);
        PiersEvent.Listen(PiersEventKey.EventKey.LootCollected, OnLootCollected);
    }

    private void OnLootCollected()
    {
        _goldLootDropsCollected += 2;
    }

    public Dictionary<EEnemyID, int> GetDeathLog()
    {
        return _enemyDeathLog;
    }

    public int GetGoldPickedUp()
    {
        return _goldLootDropsCollected;
    }

    public int GetGoldEarned()
    {
        return _goldEarned;
    }

    private void OnEnemyDied(EEnemyID enemyID)
    {
        if (_enemyDeathLog.ContainsKey(enemyID))
        {
            _enemyDeathLog[enemyID]++;
        }
        else
        {
            _enemyDeathLog.Add(enemyID, 1);
        }
    }
}
