using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuMiniWave : MonoBehaviour
{
    [SerializeField] private List<Sprite> _possibleWaveSprites;
    [SerializeField] private Image _image;

    private bool _isSetup;
    private float _speed;
    private float _riseHeight = 50;
    private bool _hasPeaked;
    private float _originalY;

    public void Setup(float speed, float x, float y)
    {
        _originalY = y;
        transform.localPosition = new Vector3(x, y - _riseHeight, 0);
        _speed = speed;
        int spriteIndex = Random.Range(0, _possibleWaveSprites.Count - 1);
        _image.sprite = _possibleWaveSprites[spriteIndex];
        
        if(_speed < 0)
            _image.transform.localScale = new Vector3(-1,1,1);

        _isSetup = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_isSetup)
            Move();
    }

    private void Move()
    {
        if (!_hasPeaked)
            RisingMovement();
        else
            DescendingMovement();
        
        transform.localPosition += Vector3.right * Time.deltaTime * _speed;
    }

    private void RisingMovement()
    {
        transform.localPosition += Vector3.up * Time.deltaTime * Mathf.Abs(_speed);

        if (transform.localPosition.y >= _originalY)
            _hasPeaked = true;
    }

    private void DescendingMovement()
    {
        transform.localPosition += Vector3.down * Time.deltaTime * Mathf.Abs(_speed);
        if(transform.localPosition.y <= _originalY - _riseHeight * 2)
            Destroy(gameObject);
    }
}
