﻿
[System.Serializable]
public abstract class SaveFile
{
    public string _filePath;

    public void SetFilePath(string path) { _filePath = path; }

    public string GetFilePath() { return _filePath; }
}
