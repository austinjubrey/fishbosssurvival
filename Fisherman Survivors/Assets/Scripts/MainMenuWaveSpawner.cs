using UnityEngine;

public class MainMenuWaveSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _wavePrefab;
    [SerializeField] private Transform _content;

    public float _frequency;
    public int _chance;
    public float _maxWaveSpeed;
    public float _minWaveSpeed;

    private float _spawnTime;

    private float _screenHalfWidth = Screen.width / 2;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ManageSpawning();
    }

    private void ManageSpawning()
    {
        _spawnTime += Time.deltaTime;

        if (_spawnTime >= _frequency)
        {
            RollForSpawn();
            _spawnTime = 0;
        }
    }

    private void RollForSpawn()
    {
        if (Random.Range(0, _chance) == 0)
        {
            SpawnMiniWave();
        }
    }

    private void SpawnMiniWave()
    {
        var miniWaveSpeed = Random.Range(_minWaveSpeed, _maxWaveSpeed);
        miniWaveSpeed = Random.Range(0, 2) == 0 ? miniWaveSpeed * -1 : miniWaveSpeed;

        float waveX;
        float waveY;

        if (miniWaveSpeed > 0) //going right
        {
            waveX = Random.Range(_screenHalfWidth * -1, 0);
        }
        else //going left
        {
            waveX = Random.Range(0, _screenHalfWidth);
        }

        waveY = Random.Range(60, 100);

        var gameObject = Instantiate(_wavePrefab, _content);
        MainMenuMiniWave miniWave = gameObject.GetComponent<MainMenuMiniWave>();
        miniWave.Setup(miniWaveSpeed, waveX, waveY);
    }
}
