using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private GameObject _title;
    [SerializeField] private GameObject _buttonContainer;
    [SerializeField] private GameObject _exitButton;
    [SerializeField] private TextMeshProUGUI _goldLabel;
    [SerializeField] private SettingsMenu _settingsMenu;
    [SerializeField] private ShopMenu _shopMenu;
    [SerializeField] private CharacterSelectController _characterSelect;
    [SerializeField] private FishbaseMenuController _fishbaseMenu;
    [SerializeField] private MenuPlayerCharacter _playerCharacter;
    
    [SerializeField] private GameObject _backgroundParent;
    private bool _shouldTransitionBackground;
    private float _backgroundTransitionSpeed;
    public float _transitionDelay;

    private static string GAME_SCENE_NAME = "GameScene";
    private static string TRANSITION_SCENE = "TransitionScene";

    public void OnSettingsButton()
    {
        OnHideOriginalMenu(true);
        _settingsMenu.gameObject.SetActive(true);
    }
    
    public void OnPlayButton()
    {
        _playerCharacter.TriggerJump();
        OnHideOriginalMenu(true);
    }

    public void GoToGameScene()
    {
        SceneManager.LoadSceneAsync(TRANSITION_SCENE, LoadSceneMode.Additive);
    }

    public void OnExitButton()
    {
        Application.Quit();
    }
    
    public void OnShopButton()
    {
        OnHideOriginalMenu(true);
        _shopMenu.gameObject.SetActive(true);
        _shopMenu.Reinitialize();
    }

    public void OnExitShopButton()
    {
        OnHideOriginalMenu(false);
        _shopMenu.gameObject.SetActive(false);
    }
    
    public void OnFishbaseButton()
    {
        OnHideOriginalMenu(true);
        _fishbaseMenu.gameObject.SetActive(true);
    }

    public void OnExitFishbaseButton()
    {
        OnHideOriginalMenu(false);
        _fishbaseMenu.gameObject.SetActive(false);
    }
    
    public void OnCharacterSelect()
    {
        OnHideOriginalMenu(true);
        _characterSelect.gameObject.SetActive(true);
        _characterSelect.Reinitialize();
    }

    public void OnExitCharacterSelect()
    {
        OnHideOriginalMenu(false);
        _characterSelect.gameObject.SetActive(false);
    }

    private void OnHideOriginalMenu(bool shouldHide)
    {
        //_title.SetActive(!shouldHide);
        _buttonContainer.SetActive(!shouldHide);
        _exitButton.SetActive(!shouldHide);
    }

    private void OnExitSettingsMenu()
    {
        OnHideOriginalMenu(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        _playerCharacter.SetCallback(GoToGameScene);
        SetupMenuCharacter();
        _settingsMenu.SetCallback(OnExitSettingsMenu);
        UpdateGoldLabel();
        PiersEvent.Listen(PiersEventKey.EventKey.PlayerSaveUpdated, OnSaveUpdated);
        PiersEvent.Listen<float>(PiersEventKey.EventKey.TransitionToGameBegin, OnTransitionToGame);
        PiersEvent.Listen(PiersEventKey.EventKey.SelectedCharacterChanged, SetupMenuCharacter);
    }

    private void SetupMenuCharacter()
    {
        var characterID = SaveManager.Instance.GetPlayerSave().GetSelectedCharacterID();
        var data = DataLibrary.Instance.GetCharacterDataByID(characterID);
        _playerCharacter.SetCharacter(data);
    }

    private void OnTransitionToGame(float speed)
    {
        _backgroundTransitionSpeed = speed;
        StartCoroutine(nameof(DelayedTransitionToGame));
    }

    private IEnumerator DelayedTransitionToGame()
    {
        yield return new WaitForSeconds(_transitionDelay);
        _shouldTransitionBackground = true;
    }

    private void Update()
    {
        if(_shouldTransitionBackground)
            _backgroundParent.transform.position += Vector3.up * Time.deltaTime * _backgroundTransitionSpeed;
    }

    private void OnSaveUpdated()
    {
        UpdateGoldLabel();
    }

    private void UpdateGoldLabel()
    {
        _goldLabel.text = SaveManager.Instance.GetPlayerSave().GetGold().ToString();
    }
}
