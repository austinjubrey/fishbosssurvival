using UnityEngine;

public class BasicProjectile : MonoBehaviour
{
    protected Vector3 _direction;
    protected float _speed = 10;
    protected float _damage = 8;
    protected float _range = 3;
    protected float _currentLifeTime;
    protected Player _owner;
    protected int _numEnemiesHit;
    protected int _maxEnemiesPierce;

    [SerializeField] protected SpriteRenderer _spriteRenderer;
    [SerializeField] protected BoxColliderHelper _collisionHelper;

    public virtual void SetTargetLocation(Vector3 targetLocation)
    {
        _direction = (transform.position - targetLocation).normalized;
    }

    public void SetEnemyPierce(int amount)
    {
        _maxEnemiesPierce = amount;
    }

    public void SetDamage(float damage)
    {
        _damage = damage;
    }

    public void SetScale(float scale)
    {
        _spriteRenderer.transform.localScale = new Vector2(scale, scale);
    }

    public virtual void SetRange(float range)
    {
        _range = range;
    }

    public void SetOwner(Player player)
    {
        _owner = player;
    }

    public void Start()
    {
        _collisionHelper.SetTriggerCallback(OnTriggerEnter2D);
    }

    // Update is called once per frame
    protected void Update()
    {
        ManageLifeTime();
        transform.position += -_direction * Time.deltaTime * _speed;
    }

    protected void ManageLifeTime()
    {
        _currentLifeTime += Time.deltaTime;
        if (_currentLifeTime >= _range)
        {
            OnEndLifeTime();
        }
    }

    protected virtual void OnEndLifeTime()
    {
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other == null)
            return;
        
        var enemy = other.gameObject.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;

        _numEnemiesHit++;
        other.gameObject.GetComponentInParent<Enemy>().OnHit(GetDamage());
    }

    public float GetDamage()
    {
        //could also be aware of player buffs, accessories which boost damage
        return _damage;
    }
}
