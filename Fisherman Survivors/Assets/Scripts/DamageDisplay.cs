using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _numberLabel;
    [SerializeField] private Animator _animator;
    [SerializeField] private AnimationHelper _animationHelper;

    private bool _destroyOnAnimationEnd;
    private bool _isAnimating;
    private bool _isSpawningChild;

    public void SetAndPlay(float value)
    {
        transform.rotation = Quaternion.identity;
        
        if (!_isAnimating)
        {
            _numberLabel.text = value.ToString();
            _animator.SetTrigger("TakeDamage");
            _isAnimating = true;
        }
        else
        {
            _isSpawningChild = true;
            SetAndPlayAndDetach(value);
        }
    }

    private void SetAndPlayAndDetach(float value)
    {
        var newDisplay = Instantiate(gameObject, transform.position, Quaternion.identity);
        newDisplay.GetComponent<DamageDisplay>().SetAndPlay(value);
        newDisplay.GetComponent<DamageDisplay>().DetachAndDestroy();
        _isSpawningChild = false;

        if (_destroyOnAnimationEnd && !_isAnimating)
        {
            Destroy(gameObject);
        }
    }

    public void DetachAndDestroy()
    {
        transform.parent = null;
        _destroyOnAnimationEnd = true;
        
        if(!_isAnimating && !_isSpawningChild)
            Destroy(gameObject);
    }

    private void OnAnimationEnd()
    {
        if(_destroyOnAnimationEnd && !_isSpawningChild)
            Destroy(gameObject);
        
        _isAnimating = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _animationHelper.SetCallback(OnAnimationEnd);
    }

    // Update is called once per frame
    void Update()
    {
        if (_destroyOnAnimationEnd && !_isAnimating && !_isSpawningChild)
        {
            Destroy(gameObject);
        }
    }
}
