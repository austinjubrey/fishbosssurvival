using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterSelectItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleLabel;
    [SerializeField] private Image _image;
    [SerializeField] private Button _button;
    [SerializeField] private GameObject _selectedLabel;

    private UnityAction _callback;

    private CharacterData _data;
    
    private void SetTitleLabel(string name)
    {
        _titleLabel.text = name;
    }
    
    private void SetImage(Sprite sprite)
    {
        _image.sprite = sprite;
        _image.rectTransform.sizeDelta = sprite.bounds.size * 10;
    }
    
    public void SetupFromData(CharacterData data)
    {
        _data = data;
        SetTitleLabel(data.CharacterName);
        SetImage(data.MainMenuSprite);
        SetupButton();
    }

    public void SetCallback(UnityAction callback)
    {
        _callback = callback;
    }

    private void SetupButton()
    {
        var selectedCharacterString = SaveManager.Instance.GetPlayerSave().GetSelectedCharacterID();
        _selectedLabel.SetActive(selectedCharacterString == _data.CharacterID);
    }

    public void OnPressed()
    {
        var saveManager = SaveManager.Instance;
        if(!saveManager.GetPlayerSave().GetUnlockedCharacters().Contains(_data.CharacterID))
            saveManager.GetPlayerSave().UnlockCharacter(_data.CharacterID);
        
        saveManager.GetPlayerSave().SetSelectedCharacter(_data.CharacterID);
        saveManager.SaveGame();
        PiersEvent.Post(PiersEventKey.EventKey.SelectedCharacterChanged);
        _callback();
    }
}
