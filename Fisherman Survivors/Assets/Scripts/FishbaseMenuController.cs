using System;
using System.Collections.Generic;
using UnityEngine;

public class FishbaseMenuController : MonoBehaviour
{
    [SerializeField] private FishbaseMenuItem _displayItemPrefab;
    [SerializeField] private Transform _content;
    
    [SerializeField] private GameObject _fishTab;
    [SerializeField] private GameObject _toolsTab;
    [SerializeField] private GameObject _weaponsTab;
    [SerializeField] private GameObject _charactersTab;

    private List<EWeaponID> _weaponsToDisplay;
    private List<EToolID> _toolsToDisplay;
    private List<EEnemyID> _enemiesToDisplay;
    private List<ECharacterID> _charactersToDisplay;

    private void Start()
    {
        _weaponsToDisplay = new List<EWeaponID>();
        _toolsToDisplay = new List<EToolID>();
        _enemiesToDisplay = new List<EEnemyID>();
        _charactersToDisplay = new List<ECharacterID>();
        
        foreach (var weaponID in Enum.GetValues(typeof(EWeaponID)))
        {
            _weaponsToDisplay.Add((EWeaponID)weaponID);
        }
        
        foreach (var toolID in Enum.GetValues(typeof(EToolID)))
        {
            _toolsToDisplay.Add((EToolID)toolID);
        }
        
        foreach (var enemyID in Enum.GetValues(typeof(EEnemyID)))
        {
            _enemiesToDisplay.Add((EEnemyID)enemyID);
        }
        
        foreach (var characterID in Enum.GetValues(typeof(ECharacterID)))
        {
            _charactersToDisplay.Add((ECharacterID)characterID);
        }

        OnFishTabPressed();
    }

    private void CreateMenuItem(EWeaponID weaponID)
    {
        var data = DataLibrary.Instance.GetWeaponDataByID(weaponID);
        var instance = Instantiate(_displayItemPrefab, _content);
        var menuItem = instance.GetComponent<FishbaseMenuItem>();
        menuItem.Setup(data);
    }
    
    private void CreateMenuItem(EEnemyID enemyID)
    {
        var data = DataLibrary.Instance.GetEnemyDataByID(enemyID);
        var instance = Instantiate(_displayItemPrefab, _content);
        var menuItem = instance.GetComponent<FishbaseMenuItem>();
        menuItem.Setup(data);
    }
    
    private void CreateMenuItem(EToolID toolID)
    {
        var data = DataLibrary.Instance.GetToolDataByID(toolID);
        var instance = Instantiate(_displayItemPrefab, _content);
        var menuItem = instance.GetComponent<FishbaseMenuItem>();
        menuItem.Setup(data);
    }
    
    private void CreateMenuItem(ECharacterID characterID)
    {
        var data = DataLibrary.Instance.GetCharacterDataByID(characterID);
        var instance = Instantiate(_displayItemPrefab, _content);
        var menuItem = instance.GetComponent<FishbaseMenuItem>();
        menuItem.Setup(data);
    }

    public void OnFishTabPressed()
    {
        _fishTab.transform.SetAsLastSibling();
        
        GeneralUtility.UnParentAndDestroyChildren(_content);

        foreach (var data in _enemiesToDisplay)
        {
            CreateMenuItem(data);
        }
    }
    
    public void OnToolsTabPressed()
    {
        _toolsTab.transform.SetAsLastSibling();
        
        GeneralUtility.UnParentAndDestroyChildren(_content);

        foreach (var data in _toolsToDisplay)
        {
            CreateMenuItem(data);
        }
    }
    
    public void OnWeaponsTabPressed()
    {
        _weaponsTab.transform.SetAsLastSibling();
        
        GeneralUtility.UnParentAndDestroyChildren(_content);

        foreach (var data in _weaponsToDisplay)
        {
            CreateMenuItem(data);
        }
    }
    
    public void OnCharactersTabPressed()
    {
        _charactersTab.transform.SetAsLastSibling();
        
        GeneralUtility.UnParentAndDestroyChildren(_content);

        foreach (var data in _charactersToDisplay)
        {
            CreateMenuItem(data);
        }
    }
}
