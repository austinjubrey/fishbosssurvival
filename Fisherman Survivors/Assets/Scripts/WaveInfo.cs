using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveInfo
{
    [Tooltip("Main enemy spawned in this wave")]public EnemyData EnemyData;
    [Tooltip("Wave will an amount of clusters between min and max (min inclusive, max exclusive)")]public Vector2 ClusterSpawnRange = new Vector2(1,3);
    [Tooltip("Wave will spawn an amount of enemies per cluster between min and max (min inclusive, max exclusive)")]public Vector2 ClusterSizeRange = new Vector2(1,3);

    [Tooltip("Base enemy melee damage will be multiplied by this value")][Min(1)]public float DamageMultiplier;
    [Tooltip("Base enemy health will be multiplied by this value")][Min(1)]public float HealthMultiplier;
    //this would contain a list of extra units which show up in the round, or wtv.
}
