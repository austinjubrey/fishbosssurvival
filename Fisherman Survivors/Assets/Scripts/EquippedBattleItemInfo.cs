using UnityEngine;

public class EquippedBattleItemInfo
{
    public static float FIRE_RATE_FLOOR = 0.5f;
    protected string _name;
    protected int _level; //one indexed
    protected int _maxLevel;
    
    protected float _damage;
    protected float _fireRate;
    protected float _range;
    protected float _sizeModifier;
    protected int _extraProjectiles;
    protected int _enemyPierce;
    
    public float Damage => _damage;
    public float FireRate => _fireRate;
    public float Range => _range;
    public float SizeModifier => _sizeModifier;
    public string Name => _name;
    public int Level => _level;
    public int MaxLevel => _maxLevel;
    public int ExtraProjectiles => _extraProjectiles;
    public int EnemyPierce => _enemyPierce;
    
    public virtual void SetData(BattleItemData data, int level = 1)
    {
        _level = level;
        _maxLevel = data._levelUpInfo._upgradeTiers.Count;
        _fireRate = data.BaseFireRate;
        _range = data.BaseRange;
        _name = data.Name;
        _damage = data.BaseDamage;
        _extraProjectiles = data.BaseExtraProjectiles;
        _enemyPierce = data.BaseEnemyPierce;
        _sizeModifier = data.BaseSizeModifier;
    }
    
    public void LevelUp()
    {
        if (_level - 1 < _maxLevel)
        {
            _level++;
            UpdateStats();
            if (_level - 1 == _maxLevel)
            {
                //Max level reached
            }
        }
    }
    
    protected virtual void UpdateStats()
    {
    }
}
