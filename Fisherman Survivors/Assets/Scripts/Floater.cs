using UnityEngine;

public class Floater : MonoBehaviour
{
    public float _bobAmplitude;
    public float _bobPeriod;
    
    public float _tiltSpeed;
    public float _tiltLimit;

    public bool _disableBob;
    
    private Vector3 startPos;
    
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        PiersEvent.Listen<float>(PiersEventKey.EventKey.TransitionToGameBegin, OnTransitionScene);
    }

    private void OnTransitionScene(float speed)
    {
        _disableBob = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(!_disableBob)
            ManageVerticalFloating();
        
        ManageFloatingRotation();
    }

    private void ManageVerticalFloating()
    {
        float theta = Time.timeSinceLevelLoad / _bobPeriod;
        float distance = _bobAmplitude * Mathf.Sin(theta);
        transform.position = startPos + Vector3.up * distance;
    }

    private void ManageFloatingRotation()
    {
        float rZ = Mathf.SmoothStep(-_tiltLimit,_tiltLimit,Mathf.PingPong(Time.time * _tiltSpeed,1));
        transform.rotation = Quaternion.Euler(0,0,rZ);
    }
}
