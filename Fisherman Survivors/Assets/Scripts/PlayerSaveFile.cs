﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSaveFile : SaveFile
{
    [SerializeField] private int _gold;
    
    //shop items
    [SerializeField] private int _fishingLineUpgrades; //the amount of "fishing line" upgrades the player has
    [SerializeField] private bool _hasUnlockedHook; //if TRUE fishing rod weapons will hook
    [SerializeField] private int _healthUpgrades; //number of health upgrades bought in the shop
    [SerializeField] private int _damageUpgrades; //number of damage upgrades bought in the shop
    [SerializeField] private int _goldIncreaseUpgrades; //number of gold increase upgrades bought in the shop
    [SerializeField] private int _xpIncreaseUpgrades; //number of XP increase upgrades bought in the shop
    [SerializeField] private int _movementSpeedUpgrades; //number of movement speed upgrades bought in the shop
    
    [SerializeField] private List<string> _unlockedCharacters = new List<string>();
    [SerializeField] private string _selectedCharacter;

    //Getters
    public int GetGold() { return _gold; }
    public int GetFishingLineUpgrades() { return _fishingLineUpgrades; }
    public bool GetHasUnlockedHook() { return _hasUnlockedHook; }
    public int GetHealthUpgrades() { return _healthUpgrades; }
    public int GetDamageUpgrades() { return _damageUpgrades; }
    public int GetGoldIncreaseUpgrades() { return _goldIncreaseUpgrades; }
    public int GetXpIncreaseUpgrades() { return _xpIncreaseUpgrades; }
    public int GetMovementSpeedUpgrades() { return _movementSpeedUpgrades; }
    public List<ECharacterID> GetUnlockedCharacters()
    {
        List<ECharacterID> unlockedCharacters = new List<ECharacterID>();
        foreach (var characterString in _unlockedCharacters)
        {
            if (System.Enum.TryParse(characterString, out ECharacterID characterID))
                unlockedCharacters.Add(characterID);
        }
        return unlockedCharacters;
    }

    public ECharacterID GetSelectedCharacterID()
    {
        return System.Enum.TryParse(_selectedCharacter, out ECharacterID characterID) ? characterID : ECharacterID.Fisherman;
    }

    //Setters
    public void SetGold(int amount) { _gold = amount; }
    public void SetFishingLineUpgrade(int amount) { _fishingLineUpgrades = amount; }
    public void SetHealthUpgrades(int amount) { _healthUpgrades = amount; }
    public void SetDamageUpgrades(int amount) { _damageUpgrades = amount; }
    public void SetGoldIncreaseUpgrades(int amount) { _goldIncreaseUpgrades = amount; }
    public void SetXpIncreaseUpgrades(int amount) { _xpIncreaseUpgrades = amount; }
    public void SetMovementSpeedUpgrades(int amount) { _movementSpeedUpgrades = amount; }
    public void SetSelectedCharacter(ECharacterID characterID) { _selectedCharacter = characterID.ToString(); }

    //Adders
    public void AddGold(int amount) { _gold += amount; }
    public void AddFishingLineUpgrade() { _fishingLineUpgrades++; }
    public void AddHealthUpgrade() { _healthUpgrades++; }
    public void AddDamageUpgrade() { _damageUpgrades++; }
    public void AddGoldIncreaseUpgrade() { _goldIncreaseUpgrades++; }
    public void AddXpIncreaseUpgrade() { _xpIncreaseUpgrades++; }
    public void AddMovementSpeedUpgrade() { _movementSpeedUpgrades++; }
    public void UnlockHook() { _hasUnlockedHook = true; }
    public void UnlockCharacter(ECharacterID ID)
    {
        var stringID = ID.ToString();
        if(!_unlockedCharacters.Contains(stringID))
            _unlockedCharacters.Add(stringID);
    }
}
