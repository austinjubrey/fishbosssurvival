﻿using System.Collections;
using UnityEngine;
using System.IO;

public class SaveManager : MonoBehaviour
{
    [SerializeField] private EconomyData _economyData;
    
    //SaveFiles
    private PlayerSaveFile _playerSaveFile;

    //Json path
    string _jsonSavePath;
    string _defaultSaveFileName = "FishBossSaveFile";
    string _defaultSaveFolderName = "FishBossSaves";
    string _jsonSuffix = ".json";
    string _emptyPath = "";

    //Singleton things
    private static SaveManager _instance;
    public static SaveManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
        _jsonSavePath = Application.persistentDataPath;
        _playerSaveFile = LoadPlayerSave();
    }

    #region Economy Functions
    
    public EconomyData GetEconomyData()
    {
        return _economyData;
    }

    public float GetUpgradedFishingLineLength()
    {
        var upgrades = _playerSaveFile.GetFishingLineUpgrades();
        return upgrades > 0 ? upgrades * _economyData.FishingLineUpgradeValue : 0f;
    }
    
    public float GetUpgradedHealthValue()
    {
        var upgrades = _playerSaveFile.GetHealthUpgrades();
        return upgrades > 0 ? upgrades * _economyData.HealthUpgradeValue : 0f;
    }
    
    public float GetUpgradedDamageValue()
    {
        var upgrades = _playerSaveFile.GetDamageUpgrades();
        return upgrades > 0 ? upgrades * _economyData.DamageUpgradeValue : 0f;
    }
    
    public float GetUpgradedGoldIncreaseValue()
    {
        var upgrades = _playerSaveFile.GetGoldIncreaseUpgrades();
        return upgrades > 0 ? upgrades * _economyData.GoldIncreaseUpgradeValue : 0f;
    }
    
    public float GetUpgradedXpIncreaseValue()
    {
        var upgrades = _playerSaveFile.GetXpIncreaseUpgrades();
        return upgrades > 0 ? upgrades * _economyData.XpIncreaseUpgradeValue : 0f;
    }
    
    public float GetUpgradedMovementSpeedValue()
    {
        var upgrades = _playerSaveFile.GetMovementSpeedUpgrades();
        return upgrades > 0 ? upgrades * _economyData.MovementSpeedUpgradeValue : 0f;
    }

    #endregion

    public void ResetSave()
    {
        _playerSaveFile = new PlayerSaveFile();
        SaveGame();
    }

    public PlayerSaveFile GetPlayerSave()
    {
        return _playerSaveFile;
    }

    private string GenerateSavePath()
    {

        string pathToUse = _emptyPath;

        pathToUse = _jsonSavePath + "/" + _defaultSaveFileName + _jsonSuffix;

        return pathToUse;
    }

    public void SaveGame()
    {
        StartCoroutine(SaveRoutine());
    }

    private IEnumerator SaveRoutine()
    {
        yield return UpdatePlayerSaveFile();
    }

    private IEnumerator UpdatePlayerSaveFile()
    {
        PlayerSaveFile newSave = _playerSaveFile;
        string jsonData = JsonUtility.ToJson(newSave);

        File.WriteAllText(GenerateSavePath(), jsonData);
        PiersEvent.Post(PiersEventKey.EventKey.PlayerSaveUpdated);

        return null;
    }

    private PlayerSaveFile LoadPlayerSave()
    {
        var savePath = GenerateSavePath();
        
        if (File.Exists(savePath))
        {
            return JsonUtility.FromJson<PlayerSaveFile>(File.ReadAllText(savePath));
        }

        return new PlayerSaveFile();
    }
}
