using System;
using System.Collections.Generic;
using UnityEngine;

public class DataLibrary : MonoBehaviour
{
    [Header("Weapon Data")]
    [SerializeField] private WeaponData _fishingRodData;
    [SerializeField] private WeaponData _harpoonData;
    [SerializeField] private WeaponData _netData;
    [SerializeField] private WeaponData _BasicTestWeapon;
    [SerializeField] private WeaponData _tridentData;
    [SerializeField] private WeaponData _whirlyRodData;
    [SerializeField] private WeaponData _throwNetData;
    [SerializeField] private WeaponData _anchorData;
    [SerializeField] private WeaponData _seaweedSwordData;
    
    [Header("Tool Data")]
    [SerializeField] private ToolData _tackleBoxData;
    [SerializeField] private ToolData _blastPlate;
    [SerializeField] private ToolData _harpoonOil;
    [SerializeField] private ToolData _hookSharpener;
    
    [Header("Enemy Data")]
    [SerializeField] private EnemyData _baseEnemy;
    [SerializeField] private EnemyData _lilGreen;
    [SerializeField] private EnemyData _blobby;
    [SerializeField] private EnemyData _babyShark;
    
    [Header("Character Data")]
    [SerializeField] private CharacterData _fisherman;
    [SerializeField] private CharacterData _fishPerson;

    private List<WeaponData> _weaponDataList;
    private List<ToolData> _toolDataList;
    private List<EnemyData> _enemyDataList;
    private List<CharacterData> _characterDataList;
    
    private static DataLibrary _instance;
    public static DataLibrary Instance { get { return _instance; } }

    public List<WeaponData> GetWeaponPool() => _weaponDataList;
    public List<ToolData> GetToolPool() => _toolDataList;
    public List<EnemyData> GetEnemyPool() => _enemyDataList;
    public List<CharacterData> GetCharacterPool() => _characterDataList;
    
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
        _weaponDataList = new List<WeaponData>();
        _toolDataList = new List<ToolData>();
        _enemyDataList = new List<EnemyData>();
        _characterDataList = new List<CharacterData>();
        
        //WeaponData
        foreach (var weaponID in Enum.GetValues(typeof(EWeaponID)))
        {
            _weaponDataList.Add(GetWeaponDataByID((EWeaponID)weaponID));
        }
        
        //ToolData
        foreach (var toolID in Enum.GetValues(typeof(EToolID)))
        {
            _toolDataList.Add(GetToolDataByID((EToolID)toolID));
        }
        
        //EnemyData
        foreach (var enemyID in Enum.GetValues(typeof(EEnemyID)))
        {
            _enemyDataList.Add(GetEnemyDataByID((EEnemyID)enemyID));
        }
        
        //CharacterData
        foreach (var characterID in Enum.GetValues(typeof(ECharacterID)))
        {
            _characterDataList.Add(GetCharacterDataByID((ECharacterID)characterID));
        }
    }

    public WeaponData GetWeaponDataByID(EWeaponID ID)
    {
        switch (ID)
        {
            case EWeaponID.FishingRod:
                return _fishingRodData;
            case EWeaponID.Harpoon:
                return _harpoonData;
            case EWeaponID.Net:
                return _netData;
            case EWeaponID.BasicTestWeapon:
                return _BasicTestWeapon;
            case EWeaponID.Trident:
                return _tridentData;
            case EWeaponID.WhirlyRod:
                return _whirlyRodData;
            case EWeaponID.ThrowNet:
                return _throwNetData;
            case EWeaponID.Anchor:
                return _throwNetData; //todo implement anchor data
            case EWeaponID.SeaweedSword:
                return _seaweedSwordData;
            default:
                throw new ArgumentOutOfRangeException(nameof(ID), ID, null);
        }
    }

    public ToolData GetToolDataByID(EToolID ID)
    {
        switch (ID)
        {
            case EToolID.TackleBox:
                return _tackleBoxData;
            case EToolID.BlastPlate:
                return _blastPlate;
            case EToolID.HarpoonOil:
                return _harpoonOil;
            case EToolID.HookSharpener:
                return _hookSharpener;
            default:
                throw new ArgumentOutOfRangeException(nameof(ID), ID, null);
        }
    }
    
    public EnemyData GetEnemyDataByID(EEnemyID ID)
    {
        switch (ID)
        {
            case EEnemyID.BaseEnemy:
                return _baseEnemy;
            case EEnemyID.lilGreen:
                return _lilGreen;
            case EEnemyID.BrownBlobby:
                return _blobby;
            case EEnemyID.BabyShark:
                return _babyShark;
            default:
                throw new ArgumentOutOfRangeException(nameof(ID), ID, null);
        }
    }
    
    public CharacterData GetCharacterDataByID(ECharacterID ID)
    {
        switch (ID)
        {
            case ECharacterID.Fisherman:
                return _fisherman;
            case ECharacterID.FishPerson:
                return _fishPerson;
            default:
                throw new ArgumentOutOfRangeException(nameof(ID), ID, null);
        }
    }
}
