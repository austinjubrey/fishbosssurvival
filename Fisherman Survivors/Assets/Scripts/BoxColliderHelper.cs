using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoxColliderHelper : MonoBehaviour
{
    private UnityAction<Collider2D> _triggerCallback;
    private UnityAction<Collision2D> _collisionCallback;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetTriggerCallback(UnityAction<Collider2D> callback)
    {
        _triggerCallback = callback;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_triggerCallback == null)
            return;

        _triggerCallback(other);
    }
    
    public void SetCollisionCallback(UnityAction<Collision2D> callback)
    {
        _collisionCallback = callback;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (_collisionCallback == null)
            return;

        _collisionCallback(other);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
