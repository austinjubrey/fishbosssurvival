using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public void OnMainMenuButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void OnExitGameButton()
    {
        SaveManager.Instance.SaveGame();
        Application.Quit();
    }
}
