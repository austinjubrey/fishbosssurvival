using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [SerializeField] private GameObject _objectToSpin;
    
    public float _speed = 3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _objectToSpin.transform.Rotate(Vector3.forward, _speed);
    }
}
