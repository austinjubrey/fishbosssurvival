using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuPlayerCharacter : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Animator _animator;

    private const string JumpOffTrigger = "JumpOff";

    private UnityAction _callback;

    public void SetCallback(UnityAction callback)
    {
        _callback = callback;
    }
    
    public void SetCharacter(CharacterData data)
    {
        _image.sprite = data.MainMenuSprite;
        _image.rectTransform.sizeDelta = data.MainMenuSprite.bounds.size * 10; //I think 10 is because the sprites are set as 10 pixels per unit, but i didnt test further
    }
    
    public void TriggerJump()
    {
        _animator.SetTrigger(JumpOffTrigger);
    }

    public void OnJumpEnd()
    {
        _callback();
    }
}
