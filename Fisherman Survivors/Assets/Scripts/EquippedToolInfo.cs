using UnityEngine;

public class EquippedToolInfo : EquippedBattleItemInfo
{
    private ToolData _toolData;
    private EToolID _toolID;

    public EToolID ToolID => _toolID;
    
    public override void SetData(BattleItemData data, int level = 1)
    {
        base.SetData(data);
        ToolData toolData = data as ToolData;
        _toolData = toolData;
        _toolID = toolData.ToolID;
    }
    
    protected override void UpdateStats()
    {
        var weaponUpgradeValue = SaveManager.Instance.GetEconomyData().DefaultWeaponUpgradeValue;
        
        _damage = _toolData.BaseDamage + WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.Damage) * weaponUpgradeValue;
        _fireRate = CalculateFireRate();
        _range = _toolData.BaseRange + WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.Range) * weaponUpgradeValue;
        _extraProjectiles = _toolData.BaseExtraProjectiles + (int)WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.ExtraProjectiles);
        _enemyPierce = _toolData.BaseEnemyPierce + (int) WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.ExtraPierce);
        _sizeModifier = CalculateSize();
    }
    
    private float CalculateSize()
    {
        var upgradeValue = SaveManager.Instance.GetEconomyData().SizeModifierUpgradeValue;
        var sizeModifier = _toolData.BaseSizeModifier + WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.SizeModifier) *
            upgradeValue;

        return sizeModifier;
    }
    
    private float CalculateFireRate()
    {
        var upgradeValue = SaveManager.Instance.GetEconomyData().FireRateUpgradeValue;
        
        var fireRate = _toolData.BaseFireRate + WeaponDataHelper.GetBattleItemStatAtLevel(_toolData, _level - 1, EUpgradeType.FireRate) * upgradeValue;
        _fireRate = _fireRate < FIRE_RATE_FLOOR ? FIRE_RATE_FLOOR : _fireRate;

        return fireRate;
    }
}
