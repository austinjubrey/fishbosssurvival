
public class EquippedWeaponInfo : EquippedBattleItemInfo
{
    private WeaponData _weaponData;
    private EWeaponID _weaponID;
    public EWeaponID WeaponID => _weaponID;

    public override void SetData(BattleItemData data, int level = 1)
    {
        base.SetData(data);
        
        WeaponData weaponData = data as WeaponData;
        _weaponData = weaponData;
        _weaponID = weaponData.WeaponID;
    }

    protected override void UpdateStats()
    {
        var weaponUpgradeValue = SaveManager.Instance.GetEconomyData().DefaultWeaponUpgradeValue;
        
        _damage = _weaponData.BaseDamage + WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.Damage) * weaponUpgradeValue;
        _fireRate = CalculateFireRate();
        _range = _weaponData.BaseRange + WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.Range) * weaponUpgradeValue;
        _extraProjectiles = _weaponData.BaseExtraProjectiles + (int)WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.ExtraProjectiles);
        _enemyPierce = _weaponData.BaseEnemyPierce + (int) WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.ExtraPierce);
        _sizeModifier = CalculateSize();
    }

    private float CalculateSize()
    {
        var upgradeValue = SaveManager.Instance.GetEconomyData().SizeModifierUpgradeValue;
        var sizeModifier = _weaponData.BaseSizeModifier + WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.SizeModifier) *
            upgradeValue;

        return sizeModifier;
    }

    private float CalculateFireRate()
    {
        var upgradeValue = SaveManager.Instance.GetEconomyData().FireRateUpgradeValue;
        
        var fireRate = _weaponData.BaseFireRate + WeaponDataHelper.GetBattleItemStatAtLevel(_weaponData, _level - 1, EUpgradeType.FireRate) * upgradeValue;
        _fireRate = _fireRate < FIRE_RATE_FLOOR ? FIRE_RATE_FLOOR : _fireRate;

        return fireRate;
    }
}
