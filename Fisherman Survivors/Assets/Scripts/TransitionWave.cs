using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TransitionWave : MonoBehaviour
{
    [SerializeField] private List<Sprite> _possibleWaveSprites;
    [SerializeField] private Image _image;
    public float Speed = 50;
    public UnityAction Callback;
    public int spriteIndex = 0;
    
    private float _screenHeight = Screen.height + 300; //the extra is so the image clears the top of the screen
    
    // Start is called before the first frame update
    void Start()
    {
        _image.sprite = _possibleWaveSprites[spriteIndex];
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.up * Time.deltaTime * Mathf.Abs(Speed);

        if (Callback != null && transform.position.y > _screenHeight)
            Callback();
    }
}
