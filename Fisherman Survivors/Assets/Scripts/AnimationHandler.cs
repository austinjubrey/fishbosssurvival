﻿using System.Linq;
using UnityEditor;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _targetImage;
    [SerializeField] private Texture2D _texture;
    [SerializeField] private float _defaultFrameInterval = 0.5f;
    [SerializeField] private BoxCollider2D _boxCollider;
    private float _currentFrameInterval;
    private bool _shouldAnimate;
    private int _frameIndex;
    private int _maxFrames;

    private Sprite[] _sprites;
    
    // Start is called before the first frame update
    void Start()
    {
        if (_texture != null)
            SetTexture(_texture);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_shouldAnimate)
        {
            return;
        }

        _currentFrameInterval += Time.deltaTime;
        if (_currentFrameInterval >= _defaultFrameInterval)
        {
            _currentFrameInterval = 0;
            OnFrameChange();
        }
    }

    private void OnFrameChange()
    {
        _frameIndex++;
        if (_frameIndex >= _maxFrames)
        {
            _frameIndex = 0;
        }

        SetTargetImage(_sprites[_frameIndex]);
    }

    private void SetTargetImage(Sprite newSprite)
    {
        _targetImage.sprite = newSprite;
    }

    public void SetTexture(Texture2D newTexture)
    {
        _texture = newTexture;
        
        var spriteSheet = "Images/" + _texture.name;
        _sprites = Resources.LoadAll(spriteSheet).OfType<Sprite>().ToArray();
        
        _maxFrames = _sprites.Length;
        _shouldAnimate = true;

        if (_boxCollider == null)
            return;
        
        var size = _sprites[0].bounds.size;
        _boxCollider.size = size;
    }

    public void SetShouldAnimate(bool shouldAnimate)
    {
        _shouldAnimate = shouldAnimate;
    }

    public void SetFrameSpeed(float newSpeed)
    {
        _defaultFrameInterval = newSpeed;
    }
}
