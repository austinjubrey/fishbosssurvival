using System.Collections.Generic;
using UnityEngine;

public class ShopMenu : MonoBehaviour
{
    [SerializeField] private List<ShopItemData> _shopItemDataPool;
    [SerializeField] private GameObject _shopItemPrefab;
    [SerializeField] private GameObject _content;

    public void Reinitialize()
    {
        PopulateShopItems();
    }

    private void PopulateShopItems()
    {
        GeneralUtility.UnParentAndDestroyChildren(_content.transform);
        foreach (var data in _shopItemDataPool)
        {
            CreateShopItem(data);
        }
    }

    private void CreateShopItem(ShopItemData data)
    {
        ShopItem item = Instantiate(_shopItemPrefab, _content.transform).GetComponent<ShopItem>();
        item.SetupFromData(data);
    }
}
