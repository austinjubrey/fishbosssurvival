using UnityEngine;

public class StatsContainerController : MonoBehaviour
{
    [SerializeField] private GameObject _weaponStatsParent;
    [SerializeField] private GameObject _toolStatsParent;
    [SerializeField] private GameObject _statsEntryPrefab;
    [SerializeField] private GameObject _weaponDataContent;
    [SerializeField] private GameObject _toolDataContent;
    
    public void SetStatsActive(bool shouldBeActive)
    {
        _weaponStatsParent.SetActive(shouldBeActive);
        _toolStatsParent.SetActive(shouldBeActive);

        if (!shouldBeActive)
            return;
        
        PopulateWeaponData();
        PopulateToolData();
    }
    
    private void PopulateWeaponData()
    {
        GeneralUtility.UnParentAndDestroyChildren(_weaponDataContent.transform);
        
        var player = GameObject.FindWithTag("Player").GetComponent<Player>();
        foreach (var weaponInfo in player.GetEquippedWeaponInfo())
        {
            var entry = Instantiate(_statsEntryPrefab, _weaponDataContent.transform);
            entry.GetComponent<WeaponStatEntry>().SetData(weaponInfo);
        }
    }
    
    private void PopulateToolData()
    {
        GeneralUtility.UnParentAndDestroyChildren(_toolDataContent.transform);
        
        var player = GameObject.FindWithTag("Player").GetComponent<Player>();
        foreach (var toolInfo in player.GetEquippedToolInfo())
        {
            var entry = Instantiate(_statsEntryPrefab, _toolDataContent.transform);
            entry.GetComponent<WeaponStatEntry>().SetData(toolInfo);
        }
    }
}
